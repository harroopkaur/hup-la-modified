@extends('admin.includes.main')

@section('content') 


 <script type="text/javascript" src="{{url('/ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript" src="{{url('/ckfinder/ckfinder.js')}}"></script>
<div class="container">
    <center><h2>{{ __('messages.create Template') }}</h2></center><br>
<form action="{{url('admin/tmpsave')}}"method="post">
	{{csrf_field()}}

    <input class="form-control"placeholder="{{ __('messages.enter Template name') }}"type="text" name="name"value=" "><br>

	   <textarea  id="editor1" name="tmp" rows="10" cols="80"> </textarea>

    </p>
 
<script type="text/javascript">
var editor = CKEDITOR.replace( 'editor1', {
    filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
    filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?type=Images',
    filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
    filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
    filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
});
CKFinder.setupCKEditor( editor, '../' );
</script>
	<input type="submit" value="{{ __('messages.save') }}"name="submit"class="btn btn-info">
</form>














@endsection