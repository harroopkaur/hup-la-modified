@include('admin.includes.header')
@include('admin.includes.sidebar')

<div class="page-content-wrapper">
                <div class="page-content" style="overflow:hidden;">
	@yield('content')
	</div>
</div>
@include('admin.includes.footer')