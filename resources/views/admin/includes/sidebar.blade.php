
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200"><br>
                        <li class="nav-item start ">
                            <a href="{{url('/admin/vminventory')}}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">{{ __('messages.vM Inventory') }}</span>
                                <span class="arrow"></span>
                            </a>
                            
                        </li>
                        <li class="nav-item start ">
                            <a href="{{url('/admin/index')}}" class="nav-link nav-toggle">
                                <i class="icon-bulb"></i>
                                <span class="title">{{ __('messages.setting') }}</span>
                                <span class="arrow"></span>
                            </a>
                            
                        </li>





                          <li class="nav-item start ">
                            <a href="{{url('/admin/tmpview')}}" class="nav-link nav-toggle">
                                <i class="fa fa-envelope"></i>
                                <span class="title">{{ __('messages.manage Mail Template') }}</span>
                                <span class="arrow"></span>
                            </a>
                            
                        </li>
                          <li class="nav-item start ">
                            <a href="{{url('/admin/category')}}" class="nav-link nav-toggle">
                                <i class="icon-notebook"></i>
                                <span class="title">{{ __('messages.user Guide') }}</span>
                                <span class="arrow"></span>
                            </a>
                            
                        </li>

<!-- 
                        <li class="nav-item start ">
                            <a href="{{url('/admin/messages')}}" class="nav-link ">
                                <i class="icon-notebook"></i>
                                <span class="title">Massages</span>
                                <span class="arrow"></span>
                            </a>
                            
                        </li> -->
                       
                       </ul>
                       
                       
                        
                       
          
   <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
