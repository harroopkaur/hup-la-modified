@extends('admin.includes.main')
 
@section('content')
<h1 class="page-title">Category Dashboard
  <small>List of Category</small>
</h1>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="icon-home"></i>
      <a href="">Home</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li class="back-btn">
			<a href="javascript: history.go(-1)">Back</a>
			<i class="fa fa-angle-right"></i>
		</li>
    <li>
     <a >Add Category</a>
   </li>
  
</ul>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add Category</div>
 
                <div class="panel-body">
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                    <form class="form-horizontal" method="POST" action="">
                        {{ csrf_field() }}
 
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="new-password" class="col-md-4 control-label">Name</label>
                            <div class="col-md-6">
                                <input id="name" type="taxt" class="form-control" name="name" required>
 
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <label for="content" class="col-md-4 control-label">Content</label>
 
                            <div class="col-md-6">
                            <textarea class="ckeditor form-control" name="content" rows="6" ></textarea>
								@if ($errors->has('content'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('content') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary green">
                                    Add Category
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection