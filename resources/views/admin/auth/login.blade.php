

@extends('admin.layout.auth')

@section('content')
<html lang="{{ app()->getLocale() }}">
<head>
<title>Portal husting</title>

  <link rel="stylesheet" href="{{url('/assets/dcloginfiles/style.css')}}">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cutive+Mono|Lato:300,400">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
   <link rel="stylesheet" href="{{url('assets/dcloginfiles/app/styles/progress-tracker.css')}}">
   
       <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cutive+Mono|Lato:300,400">

  </head>
<body>
  
<div class="col-md-12  col-sm-12 col-lg-12 col-xl-12 header">
<img src="{{url('img/logo.png')}}">
<div class="col-md-2" style="float:right;margin-top: 40px;margin-bottom:40px;">
<select name="forma" onchange="location = this.value;">
<option value="">Select Language</option>
<option value="{{ url('locale/en') }}">ENGLISH</option>
<option value="{{ url('locale/es') }}">SPANISH</option>
</select>
</div>
</div>

<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12 header1">
<div class="col-md-8">
<div class="col-md-12">
<div class="col-md-8 col-md-offset-2 box_1">
  <div class="col-md-8">
     <legend class="user_1">
{{ __('messages.total access') }}</legend><br><br>
<img src="{{url('/assets/dcloginfiles/img/pc.png')}}">
</div>
<div class="col-md-4 col-sm-12 ">
  <div class="col-md-12 col-sm-12 col-lg-12 progress_button">
 <center><button >{{ __('messages.endUsers') }}</button></center>
 <h4>{{ __('messages.access to end user app') }}</h4></div>
 <div class="col-md-12 col-sm-12 col-lg-12 progress_button_1">
  <center><button >{{ __('messages.datacenter') }}</button></center>
  <h4>{{ __('messages.vM Inventory') }}</h4></div>
  </div>
</div>  
</div>
<div class="col-md-12 col-sm-12 col-lg-12 box_2">
  <div class="col-md-4 col-sm-12 col-lg-2 heading ">
<h3 style="color: #09d96b;">{{ __('messages.process Flow') }} <br>{{ __('messages.datacenter') }}</h3></div>
<div class="col-md-8  col-md-offset-1 col-sm-12 col-lg-12 col-xl-8 te_center ">
        <ul class="progress-tracker progress-tracker--text progress-tracker--text-top">
          <li class="progress-step is-complete">
            <span class="progress-text">
        <img src="{{url('/assets/dcloginfiles/img/student.png')}}">
                  <h4 class="progress-title"{{ __('messages.log In') }}</h4>

            </span>
            <span class="progress-marker">1</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
          <img src="{{url('/assets/dcloginfiles/img/mobile-app.png')}}">
              <h4 class="progress-title">{{ __('messages.vM Inventory') }}</h4>
          
            </span>
            <span class="progress-marker">2</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
      <img src="{{url('/assets/dcloginfiles/img/enterprise.png ')}}">
              <h4 class="progress-title">{{ __('messages.endUser Management') }}</h4>
              
            </span>
            <span class="progress-marker">3</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
      <img src="{{url('/assets/dcloginfiles/img/blog.png')}}">
                 <h4 class="progress-title">{{ __('messages.endUser Request') }}</h4>
           
            </span>
            <span class="progress-marker">4</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
      <img src="{{url('/assets/dcloginfiles/img/video-conference.png')}}">
              <h4 class="progress-title">{{ __('messages.user Inventory') }}</h4>
              
            </span>
            <span class="progress-marker">5</span>
          </li>
        </ul>
  </div>  
</div>
<div class="col-md-12 col-md-offset-8 col-sm-12  arrow">
<img src="{{url('/assets/dcloginfiles/img/arrow.png')}}">
</div>
<div class="col-md-12 col-sm-12 col-lg-12  box_3">
 <div class="col-md-4 col-sm-12 col-lg-2 heading">
      <h3 style="color: #0cd4f5;">{{ __('messages.process Flow End user') }}</h3></div>
      <div class="col-md-8 col-md-offset-3  col-sm-12 col-lg-4 col-xl-8 te_center">
     <ul class="progress-tracker progress-tracker--text progress-tracker--text-top">
          <li class="progress-step is-complete">
            <span class="progress-text">
        <img src="{{url('/assets/dcloginfiles/img/student.png')}}">
                  <h4 class="progress-title">{{ __('messages.log In') }}</h4>
             
            </span>
            <span class="progress-marker own_color ">1</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
          <img src="{{url('/assets/dcloginfiles/img/blog.png')}}">
              <h4 class="progress-title">{{ __('messages.vM Inventory') }}</h4>
          
            </span>
            <span class="progress-marker own_color " >2</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
      <img src="{{url('/assets/dcloginfiles/img/video-conference.png')}}">
              <h4 class="progress-title">{{ __('messages.user Inventory') }}</h4>
              
            </span>
            <span class="progress-marker own_color " >3</span>
          </li>
        </ul>
  </div>
</div>
</div>
<style type="text/css">
  .dcloginn{
    padding-top:70px;
     padding-bottom:70px;
  }
</style>
<div class="col-md-4 box_4">
 <legend class="user"><img src="{{url('/assets/dcloginfiles/img/man-user.png')}}"> 
{{ __('messages.log In') }} </legend>
<div class="col-md-12 dcloginn">
  <center>

 <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/login') }}">
                        {{ csrf_field() }}
     <div class="input-group">
      <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
     <input id="email" type="email" class="form-control"placeholder="{{ __('messages.email') }}" name="email" value="{{ old('email') }}" autofocus>
    </div>
 @if ($errors->has('email'))
                                    <span class="">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif<br>
    <div class="input-group ">
      <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
     <input id="password" type="password" class="form-control" placeholder="{{ __('messages.password') }}"name="password"> </div>

                                @if ($errors->has('password'))
                                    <span class="">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
<br>

<center><button class="btnbtn"> {{ __('messages.log In') }}
</button></center>


</form>
</center>
</div>
</div>
</div>

<center>
<!-- Footer -->
<footer class="page-footer font-small blue pt-4 ">

 
  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2019 Copyright:
    <a href="https://mdbootstrap.com/education/bootstrap/"> HUP.com</a>
  </div>
  
  <!-- Copyright -->

</footer></center>
<script src="{{url('assets/dcloginfiles/app/scripts/site.js')}}"></script>
</body>
</html><style type="text/css">
  .btnbtn{
    color:#fff;
  font-size:16px;
  font-weight:600;
  background:#9f17f6;
  border-radius:10px;
  padding:2% 15% 2% 15%;
  }
</style>
@endsection