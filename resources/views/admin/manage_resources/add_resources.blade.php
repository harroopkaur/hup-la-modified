@extends('admin.includes.main')
 
@section('content')

<h1 class="page-title">Resources Dashboard
    <small>Add  Resources</small>
</h1>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li class="back-btn">
			<a href="javascript: history.go(-1)">Back</a>
			<i class="fa fa-angle-right"></i>
		</li>
        <li>
        <a href="{{ url('admin/resources', [$cat_id]) }}">Resources</a>
        <i class="fa fa-angle-right"></i>
        </li>
        
       
        <li>
        <a >@foreach($cat as $result)
    {{$result->name}}
    @endforeach</a>
        </li>

    </ul>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Category: @foreach($cat as $result)
    {{$result->name}}
    @endforeach</div>
 
                <div class="panel-body">
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                    <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
                        {{ csrf_field() }}
 
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Title</label>
                            <div class="col-md-6">
                                <input id="name" type="taxt" class="form-control" name="title" required>
 
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            
                        </div>
                       <!--  <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <label for="Category" class="col-md-4 control-label">Category</label>
                            <div class="col-md-6">
                                  
                                <select class="form-control" name="category" >
                                
                                <option >Select Type</option>
                                @foreach($cat as $result)
                               <option value="{{$result->id}}" />{{$result->name}}</option>
                        
                               @endforeach
                               </select>
                                @if ($errors->has('category'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                @endif
                            </div>
                            
                        </div> -->
                        <input type="hidden" name="category" value="{{$cat_id}}">
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="type" class="col-md-4 control-label">Type</label>
                            <div class="col-md-6">
                                  
                                <select class="form-control" name="type" >
                                <option >Select Type</option>
                                
                               <option value="image" />Image</option>
                               <option value="video" />Video</option>
                               <option value="pdf" />PDF</option>
                               <option value="doc" />DOC</option>
                        
                               
                               </select>
                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Image <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-4">
                                            <span class="fileinput-new select_button">  </span>
                                            <input type="file" name="image"> </span>
                                @if ($errors->has('image'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                                @endif

                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="content" class="col-md-4 control-label">Content</label>
 
                            <div class="col-md-6">
                            <textarea class="ckeditor form-control" name="content" rows="6" ></textarea>
								@if ($errors->has('content'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('content') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary green">
                                    Add Resources
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection