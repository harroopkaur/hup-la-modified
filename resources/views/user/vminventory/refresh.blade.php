@extends('user.includes.main')

@section('content')
<!-- END STYLE CUSTOMIZER -->
<!-- BEGIN PAGE HEADER-->

<div class="page-bar">
    <h4>VM Inventory</h4>
    <div class="page-toolbar">
     
</div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN DASHBOARD STATS -->

@if (session('success'))
<div class="alert alert-success">
	{{ session('success') }}
</div>
@endif
@if (session('d_success'))
<div class="alert alert-success">
	{{ session('d_success') }}
</div>
@endif
  
<div class="actions">
 
       <a href="{{ url('user/vminventory/refresh') }}" class="btn btn-lg default " > <i class="fa fa-refresh" id="ic"></i> Refresh info</a> 
    <a  data-target="#sentinfo" data-toggle="modal"  class="btn btn-lg default sent" id="sent" > <i class="fa fa-send" id="ic"></i> Sent info</a>
           
       </div>
<br></br>



<div class="portlet box purple" >
                               
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                    
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                        <th colspan="8" style="text-align: center;font-size:20px;color: #fff;background: #62008c;">Data Center</th>
                                        <th colspan="11" style="text-align: center;font-size:20px;color: #fff;background: #bf2476;">End User</th>
                                                           
                                                <tr>
                                                    <th> #&nbsp;&nbsp; </th>
                                                  <!--  <th> [] &nbsp;&nbsp;</th>-->
                                                    <th class="text-center">End User&nbsp;&nbsp; </th>
                                                    <th class="text-center"> Update&nbsp;&nbsp;Date </th>
                                                    <th class="text-center"> VM &nbsp;&nbsp;</th>
                                                    <th class="text-center"> Cores&nbsp;&nbsp; </th>
                                                    <th class="text-center"> Procs&nbsp;&nbsp; </th>
                                                    <th class="text-center"> Date&nbsp;&nbsp;Install </th>
                                                    <th class="text-center"><img  src="{{url('/media/img1/sql.png')}}"height="40px"width="90px" alt="sql"/><br>SQL</th>
                                                    <th class="text-center"><img  src="{{url('/media/img1/office.png')}}"height="40px"width="90px" alt="office"/><br>Office</th>
                                                    <th class="text-center"><img  src="{{url('/media/img1/sharepoint.png')}}"height="40px"width="90px" alt="logo"/><br>Sharepoint</th>
                                                    <th class="text-center"> <img  src="{{url('/media/img1/exchange.png')}}"height="40px"width="90px" alt="logo"/><br>Exchange</th>
                                                    <th class="text-center"><img  src="{{url('/media/img1/skype.png')}}"height="40px"width="90px" alt="logo"/><br>SkypeBiz</th>
                                                    <th class="text-center"> <img  src="{{url('/media/img1/visualstudio.png')}}"height="40px"width="90px" alt="logo"/><br>Visual Studio</th>
                                                    <th class="text-center"><img  src="{{url('/media/img1/dynamic.png')}}"height="40px"width="90px" alt="logo"/><br>Dynamic</th>
                                                    <th class="text-center"> <img  src="{{url('/media/img1/project.png')}}"height="40px"width="90px" alt="logo"/><br>Project</th>
                                                    <th class="text-center"><img  src="{{url('/media/img1/rds.png')}}"height="40px"width="90px" alt="logo"/><br>RDS</th>
                                                    <th class="text-center"><img  src="{{url('/media/img1/vision.png')}}"height="40px"width="90px" alt="logo"/><br>Visio</th>
                                                <th><input type="checkbox" id="select-all"  name="select-all"/></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @php
                                            $i = 1
                                             @endphp
                                             @foreach($invent as $data1)  
                                   <form action = "{{url('user/vminventory/update',$data1->id)}}" id="formId" class="form-horizontal" enctype="multipart/form-data" method="POST">
					                {{ csrf_field() }}
                                    
                                  <input type="hidden" name="id" value="{{$data1->id}}">
                                  
                                                <tr>
                                               
                                                    <td>  {{  $i++ }} </td>
                                                    <!--<td class="text-center" ><i class="fa fa-send"></i>SendRequest </td>-->
                                                    <td class="text-center"> {{ $data1->endUser }} </td>
                                                    <td class="text-center"> {{date('d-M-Y',strtotime($data1->updatedate))}} </td>
                                                    <td class="text-center"> {{$data1->vm}} </td>
                                                    <td class="text-center"> {{$data1->cores}} </td>
                                                    <td class="text-center"> {{$data1->procs}} </td>
                                                    <td class="text-center">{{$data1->dateinstall}}</td>
                                                    
                                                   <td > 
                                                    <select class="bs-select " name="sql" id="sql" >
                                                         <option value="{{$data1->sql}}">{{$data1->sql}} </option>
                                                    @foreach($vminvent as $data)
                                                     @if($data->sql)
                                                        <option value="{{$data->sql}}">{{$data->sql}} </option>
                                                        @endif
                                                        @endforeach  
                                                    </select>
                                               </td>

                                                    
                                                    <td> 
                                                    <select class="bs-select" name="office" id="office"  >
                                                        <option  value="{{$data1->office}}">{{$data1->office}} </option>
                                                    @foreach($vminvent as $data)
                                                     @if($data->office)
                                                        <option  value="{{$data->office}}">{{$data->office}} </option>
                                                      @endif
                                                        @endforeach
                                                    </select>
                                                 </td>
                                                    <td>
                                                    <select class="bs-select" name="sharepoint" id="sharepoint">
                                                          <option value="{{$data1->sharepoint}}">{{$data1->sharepoint}} </option>
                                                    @foreach($vminvent as $data)
                                                     @if($data->sharepoint)
                                                        <option value="{{$data->sharepoint}}">{{$data->sharepoint}} </option>
                                                        @endif
                                                        @endforeach  
                                                    </select>
                                                 </td>
                                                    <td>
                                                    <select class="bs-select" name="exchange" id="exchange">
                                                         <option value="{{$data1->exchange}}">{{$data1->exchange}} </option>
                                                    @foreach($vminvent as $data)
                                                     @if($data->exchange)
                                                        <option value="{{$data->exchange}}">{{$data->exchange}} </option>
                                                        @endif
                                                        @endforeach  
                                                    </select>
                                                  </td>
                                                    <td>
                                                    <select class="bs-select" name="skypebiz" id="skypebiz">
                                                          <option value=" {{$data1->skypebiz}} "> {{$data1->skypebiz}}  </option>
                                                    @foreach($vminvent as $data)
                                                     @if($data->skypebiz)
                                                        <option value=" {{$data->skypebiz}} "> {{$data->skypebiz}}  </option>
                                                        @endif
                                                        @endforeach
                                                    </select>
                                              </td>
                                                    <td> 
                                                    <select class="bs-select " name="visualstudio" id="visualstudio">
                                                         <option value="{{$data1->visualstudio}}">{{$data1->visualstudio}} </option>
                                                    @foreach($vminvent as $data)
                                                     @if($data->visualstudio)
                                                        <option value="{{$data->visualstudio}}">{{$data->visualstudio}} </option>
                                                        @endif
                                                        @endforeach
                                                    </select>
                                               </td>
                                                    <td>
                                                    <select class="bs-select " name="dynamics" id="dynamics">
                                                        <option value="{{$data1->dynamics}}">{{$data1->dynamics}}</option>
                                                    @foreach($vminvent as $data)
                                                     @if($data->dynamics)
                                                        <option value="{{$data->dynamics}}">{{$data->dynamics}}</option>
                                                    @endif
                                                        @endforeach
                                                    </select>
                                                 </td>
                                                    <td>
                                                    <select class="bs-select " name="project" id="project">
                                                          <option value="{{$data1->project}}">{{$data1->project}} </option>
                                                    @foreach($vminvent as $data)
                                                     @if($data->project)
                                                        <option value="{{$data->project}}">{{$data->project}} </option>
                                                        @endif
                                                        @endforeach
                                                    </select>
                                                 </td>
                                                    <td> 
                                                    <select class="bs-select " name="rds" id="rds">
                                                         <option value="{{$data1->rds}}">{{$data1->rds}} </option>
                                                    @foreach($vminvent as $data)
                                                     @if($data->rds)
                                                    <option value="{{$data->rds}}">{{$data->rds}} </option>
                                                    @endif
                                                    @endforeach
                                                    </select>
                                                </td>
                                                    <td> 
                                                    <select class="bs-select " name="visio" id="visio">
                                                         <option value="{{$data1->visio}}">{{$data1->visio}} </option>
                                                    @foreach($vminvent as $data)
                                                     @if($data->visio)
                                                        <option value="{{$data->visio}}">{{$data->visio}} </option>
                                                        @endif
                                                        @endforeach
                                                    </select>
                                                </td>
                                                 <td><input type="checkbox" name="checkbox" value="{{ $data->id }}" id="checkbox"/></td>  
                                                   
                                            
  
                                                </tr>
                                                
                                                @endforeach
                                               
                                            </tbody>
                                        </table>
                                 
                                    </div>
                                </div>
                            </div>
</div>                 



                                    


<!--sent info-->
<div id="sentinfo" class="modal fade circle" tabindex="-1" data-focus-on="input:first">

                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            <h4 class="modal-title">
        Sent info</h4>
                                        </div>
                                        <div class="modal-body">
                                           <div class="col-md-12">
    <img src="{{URL::asset('media/email.png')}}" alt="img" class="img-default" height="200px" width="100%"/>
</div><br>
<div class="col-md-12">
  <center><h4>Updated Information</h4></center>
</div>
<table border="3px solid" id="add" style="width:100%;">

      <thead>
      
        <tr class="heading">
                                                    <th>SQL</th>
                                                    <th>Office</th>
                                                    <th>Sharepoint</th>
                                                    <th>Exchange</th>
                                                    <th>SkypeBiz</th>
                                                    <th>VisualStudio</th>
                                                    <th>Dynamic</th>
                                                    <th>Project</th>
                                                    <th>RDS</th>
                                                    <th>Visio</th>
      
      </tr>
  
    </thead>

    <tbody class="apends">

<!--  @foreach($invent as $data)  

      <tr>
        <td>{{$data->sql}}</td>
        <td>{{$data->office}}</td>
        <td>{{$data->sharepoint}}</td>
        <td>{{$data->exchange}}</td>
        <td>{{$data->skypebiz}}</td>
        <td>{{$data->visualstudio}}</td>
        <td>{{$data->dynamics}}</td>
        <td>{{$data->project}}</td>
        <td>{{$data->rds}}</td>
        <td>{{$data->visio}}</td>
        
        


        
</tr>

        @endforeach-->

      
</tbody>



</table>
                                       </div>
                                        <div class="modal-footer">
                                          
                                        <button type="submit" class="btn-circle" onClick='submitDetailsForm()' data-target="#sentinfo" data-toggle="modal">Send</button>
                                        <button type="button" class="btn-circle"  class="close" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                        </div>
                                    </div>
                                  

 </form>

<!--refresh info -->
<!--<div id="stack1" class="modal fade circle" tabindex="-1" data-focus-on="input:first">

                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            <h4 class="modal-title">
        Refresh</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Your data is Updated</p>
                                       
                                        <div class="modal-footer">
                                          
                                        <button type="button" class="btn-circle">Accept</button>
                                        </div>
                                    </div> -->





<style>
  .caption {
    text-align: center;
    font-size: 22px;
    margin: 4px;
  }
    .bs-select {

height: 30px;
width: 100px;
margin: 3px;

}
.modal-backdrop.in {
    opacity: 0;
    
}
.modal{
    width:60%;
}
.modal, .modal-backdrop {
    top: 20%;
}
.btn-circle {
   
    background: #bf2476;
    height: 35px;
    color: #fff;
} 
p {

    color: #0a0a91;
}
.modal-title {
  
    background: #bf2476;
    color: #fff;
    width: 100px;
    text-align: center;
}
.btn.default:not(.btn-outline) {
 
    border-radius: 30px !important;
    background-color: #c9ccd1;
}

#ic {
    color: #bf2476;
}
element {

}
.page-sidebar .page-sidebar-menu > li > a > .title, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu > li > a > .title {

 
    font-size: 16px;
}
.btn-group-lg > .btn, .btn-lg {
  
    font-size: 15px;
}
  </style>


<script language="javascript" type="text/javascript">
    function submitDetailsForm() {
       $("#formId").submit();
    }
</script>



@endsection
