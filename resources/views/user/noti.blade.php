@extends('user.includes.main')

@section('content') 
<style type="text/css">
    .pmk{
    margin:0px!important;
    padding:0px!important;
    text-align:right;
    }
    .border-bottom{
        border-bottom:0.1px solid black;
    border-top:1px solid black;
    }
    .massagebody{
  background-image: url("https://images.unsplash.com/photo-1550895030-823330fc2551?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80");
  background-repeat:none;

}
</style>

<div class="container-fluid">
 

    <div class="row">
        <div class="col-md-2 col-lg-2 col-xl-2 col-sm-2"></div>
        <div class=" col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8  massagebody">
                <center><h3 class="text-info"><b>{{ __('messages.messages') }}</b></h3></center>
<hr/>
    <h4><b>{{$notidata[0]->name}}  <p>

  
{{$notidata[0]->massage}}
</p></b></h4>
      
            
                
            @if($conversation)

                <p > 
@foreach($conversation  as $data22)
  @if($data22->nto == 'user')
<div class="border-bottom circle" style="margin-bottom:10px!important;margin-top:10px!important;margin-left:200px!important;padding:10px!important;background:#fff!important;color:#000!important;">
    @endif
      @if($data22->nto == 'dc')
<div class="border-bottom circle" style="margin-bottom:10px!important;margin-top:10px!important;margin-right:200px!important;padding:10px!important;background:#b5ffd3!important;">
    @endif
<table class="table">
    <td class="bold">{{$data22->name}}</td>
    <td class="text-right"> {{$data22->created_at}}</td>
</table>
  
{{$data22->massage}}
</div>
@endforeach              
</p>
@endif


       
    
            <div class="card card-info">
                <h4 class="text-primary bold">{{ __('messages.write Your Reply') }} &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</h4>
                
                <div class="card-block">

                     <form class="form-inline"action="{{url('user/sendmassage',$chatparent[0]->id)}}"method="post">
{{csrf_field()}}
                    <textarea name="massage" placeholder="{{ __('messages.write your Reply here!') }}" class="pb-cmnt-textarea"autofocus     required></textarea>
                   
                       <input type="hidden" name="parent"value="{{$chatparent[0]->id}}">
                         <input type="hidden" name="dcid"value="{{$chatparent[0]->dcid}}">
                           <input type="hidden" name="userid"value="{{$chatparent[0]->userid}}">
                        <button class="btn btn-primary float-xs-right" type="submit">{{ __('messages.send') }}</button>
                    </form>
                    
                </div>
            </div>
        </div>
        <div class="col-md-2 col-lg-2 col-xl-2 col-sm-2"></div>
    </div>
</div>

<style>
    .pb-cmnt-container {
        font-family: Lato;
        margin-top: 100px;
    }

    .pb-cmnt-textarea {
        resize: none;
        padding: 20px;
        height: 130px;
        width: 100%;
        border: 1px solid #F2F2F2;
    }
</style>

@endsection