<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="{{ app()->getLocale() }}">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
        
        <title> User Login </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #2 for " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{URL::to('/')}}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{URL::to('/')}}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{URL::to('/')}}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{URL::to('/')}}/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
          <link rel="stylesheet" href="{{url('assets/landing/main.css')}}">
        <link href="{{URL::to('/')}}/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{URL::to('/')}}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{URL::to('/')}}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
       <link href="{{URL::to('/')}}/public/css/admin/login-5.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link rel="stylesheet" href="{{url('/css/style.css')}}">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cutive+Mono|Lato:300,400">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
   <link rel="stylesheet" href="{{url('/assets/apps/styles/progress-tracker.css')}}">
        
       </head>
        <style>
        .login-content .truck_logo{
        width:48%;
        }
        </style>
    <!-- END HEAD -->

    <body>
        
       <div class="container">
    <div class="row">
<div class="col-md-12 col-sm-12 col-lg-12 main-headersty">
  <div class="col-md-4">
<img class="logo-topsty" src="{{url('assets/landing/img/logo.png')}}">
</div>
<div class="col-md-6">
<ul class="top_contlinks">
                        <li><a><i class="fa fa-phone"></i>(305)851-3545</a></li>
                        <li><a href="mailto:info@licensingassurance.com" title="info@licensingassurance.com"><i class="fa fa-envelope"></i>info@licensingassurance.com</a></li>
                      
            
          
                    </ul>
</div>
<div class="col-md-2 selctlang-sty">

<select class="lang-iconchange" name="forma" onchange="location = this.value;">
<option value="">Select Language</option>
<option value="{{ url('locale/en') }}">ENGLISH</option>
<option value="{{ url('locale/es') }}">SPANISH</option>
</select>
</div>
</div>
</div>
</div>


<!-- ===============================slider data start================== -->
<nav class="navbar navbar-inverse">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
     
    </div>
    <div class="collapse navbar-collapse center-datamenu" id="myNavbar">
      <ul class="nav navbar-nav uldatacenter-menuy951">
        <li><a href="{{url('')}}">Home</a></li>        
        <li><a href="#">Data Center</a></li>
        <li><a href="#">Knowledge Base</a></li>
        <li><a href="#">Contact</a></li>
        <li><a href="#" onclick="openNav()">Login</a></li>
      </ul>
     
    </div>
  </div>
</nav>
  
  <!-- ================================= -->
  <div id="mySidebar" class="sidebar">
<div class="pop-closesty951">
  <span class="closebtn" onclick="closeNav()">×</span>
</div>
<div class="login-maindatahad">
    <p>Login</p> 
</div>
<div class="main-popdataleft">
  <ul>
      <li><a href="{{url('dcsection/')}}" class="login-s">Log In DC</a></li>
      <li><a href="{{url('user/')}}" class="login-s">Log In End User</a></li>
      <li><a href="#" class="login-s">Log In DC</a></li>
  </ul>
</div>
</div>
<!-- ===================================== -->

 <div class="main-topbanner951">
  <div class="main-layer951data">
    <div class="container">
      <div class="row">
    <h6>End User</h6>
  </div>
  </div>
  </div>
 </div>

<!-- =====================slider data close ======================== -->

<!-- ========================= -->
<div class="end-usersec1">
  <div class="container">
    <div class="row">

<div class="col-md-7  col-sm-7 col-lg-7 box_2">
  <legend class="process-flowsty951">
{{ __('messages.process Flow') }} </legend>
<div class="col-md-12 te_center">
     <ul class="progress-tracker progress-tracker--text progress-tracker--text-top">
          <li class="progress-step is-complete">
            <span class="progress-text">
         <img src="{{url('/assets/landing/img/password.png')}}">
                  <h4 class="progress-title">{{ __('messages.log In') }}</h4>
             
            </span>
            <span class="progress-marker">1</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
            <img src="{{url('/assets/landing/img/file.png')}}">
              <h4 class="progress-title">{{ __('messages.assigned License') }}
</h4>
          
            </span>
            <span class="progress-marker">2</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
     <img src="{{url('/assets/landing/img/literature.png')}}">
              <h4 class="progress-title">{{ __('messages.send to DC update') }}
</h4>
              
            </span>
            <span class="progress-marker">3</span>
          </li>
        </ul>  
		</div>
</div>
<div class="col-md-4  col-md-offset-1 col-sm-4 col-lg-4 box_4">
  <div class="form-maindatabg951">
    <div class="form-loginsty951">
    <h6> {{ __('messages.log In') }} </h6>
  </div>
<div class="col-md-12">
<form action="{{ url('/user/login') }}" class="login-form" method="post" role="form">
                            {{ csrf_field() }}
     <div class="input-group login-forminputsty form-marsty951 {{ $errors->has('Usuario') ? ' has-error' : '' }}">
      <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
      <input class="form-control" type="text" id="Usuario" placeholder="{{ __('messages.email') }}" name="email" value="{{ old('email') }}" autofocus> 
      @if ($errors->has('Usuario'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('Usuario') }}</strong>
                                        </span>
                                    @endif
    </div>
    
    <div class="input-group login-forminputsty form-marsty951 {{ $errors->has('Contraseña') ? ' has-error' : '' }}">
      <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
      <input class="form-control" type="password" id="Contraseña" placeholder="{{ __('messages.password') }}" name="password"> 
      @if ($errors->has('Contraseña'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('Contraseña') }}</strong>
                                        </span>
                                    @endif   </div>
    
    
    


<!--  <center><h6 style="color:#271c5c;">{{ __('messages.forgot your password?') }}
</h6></center> -->
<center> <button type="submit" class="login-dcsty">{{ __('messages.login') }}
</button>
</center>

<div class="create-newacountsty951">
  <a href="#">{{ __('messages.forgot your password?') }}</a>
</div>

</form>
</div>
<div class="clearfix"></div>
</div>
</div>
<!-- ======================================== -->
</div>
</div>
</div>
<!-- =================================== -->
<div class="end-usersec2">
  <div class="container">
    <div class="'row">

<div class="col-md-12 col-sm-12 col-lg-12">

  <legend class="process-flowsty951">
 {{ __('messages.manage your Information in Seconds') }}
 </legend>


<div class="col-md-12 col-sm-12 col-lg-12 ">
  <div class="row text-center slideanim">
    <div class="col-md-4">
      <div class="cal-datafooter951">
        <img src="{{url('/assets/landing/img/calendar-image.png')}}" alt="Paris" >
 
        <h4>{{ __('messages.track expiration of Contracts and Notifications') }}</h4>
       </div>
     </div>
     
    <div class="col-md-4">
    <div class="cal-datafooter951">   
        <img src="{{url('/assets/landing/img/graff655.png')}}" alt="New York" >
        <h4>{{ __('messages.see reports and graphics of your VMs') }}</h4>
      </div>
    </div>

    <div class="col-md-4">
    <div class="cal-datafooter951"> 
        <img src="{{url('/assets/landing/img/graffs2.png')}}" alt="San Francisco" >
        <h4>{{ __('messages.send your update to your provider') }}</h4> 

    </div>
  </div>

  </div>
</div>



</div>
</div>
</div>
</div>
<!-- ============================================== -->

<footer style="background-color: rgb(14, 94, 194);padding: 15px; width: 100%; display: inline-block;">
  <div class="container">
  <div class="row" style="margin: 0; width: 100%">
    <div class="col-lg-3 col-md-12 dv1150_100619 logofooter-center">
      <div class="logo-footersty">
       <img class="img-footerlogo" src="{{url('assets/landing/img/footerlogo.png')}}">
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <p class="footer_heading1">Our Products</p>
      <ul class="footer_links">
        <li>
          <a class="footer-links" target="_blank" href="https://www.licensingassurance.com/SAMasaService.php">SAM as a Service</a>
        </li>
        <li>
          <a class="footer-links" target="_blank" href="https://www.licensingassurance.com/auditDefense.php">Audit Defense</a>
        </li>
        <li>
          <a class="footer-links" target="_blank" href="https://www.licensingassurance.com/SPLA.php">SAM as a Service for SPLA</a>
        </li>
        <li>
          <a class="footer-links" target="_blank" href="https://www.licensingassurance.com/deployment.php">Deployment Diagnostic</a>
        </li>
        <li>
          <a class="footer-links" target="_blank" href="https://www.licensingassurance.com/overview.php">Overview</a>
        </li>
      </ul>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <p class="footer_heading1">Our Team</p>
      <ul class="footer_links">
        <li>
          <a class="footer-links" target="_blank" href="https://www.licensingassurance.com/aboutUs.php">About Us</a>
        </li>
        <li>
          <a class="footer-links" target="_blank" href="https://www.licensingassurance.com/contact.php">Contact Us</a>
        </li>
        <li>
          <a class="footer-links" target="_blank" href="https://www.licensingassurance.com/articles.php">Article</a>
        </li>
        <li>
          <a class="footer-links" target="_blank" href="#">Videos</a>
        </li>
      </ul>
    </div>
    <div class="col-lg-3 col-md-12 col-sm-12 padding-leftzero">
      <p class="footer_heading1">Contact Us!!</p>

      <ul class="footer_txt">
        <li>
          <span>Phone: (305)851-3545</span>
        </li>
        <li>
          <span>info@licensingassurance.com</span>
        </li>
      </ul>

      <ul class="footer_txt">
        <li>
          <span>Licensing Assurance LLC</span>
        </li>
        <li>
          <span>16192 Coastal Highway Lewes, DE 19958</span>
        </li>
      </ul>

      <ul class="footer_txt">
        <li>
          <span>We are available</span>
        </li>
        <li class="pad_l_15">
          <span>Monday - Friday</span>
        </li>
        <li class="pad_l_15">
          <span>8:00 am - 5:00 pm</span>
        </li>
        <li class="pad_l_15">
          <span>East Time</span>
        </li>
      </ul>

    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 footer_copy_right">
      <p>Copyright Hosting Usage Portal. All Rights Reserved</p>
    </div>
  </div>
</div>
</footer>

</body>
</html>
        
        
        
        
        
        
      
      


       


        <!-- END : LOGIN PAGE 5-1 -->
        <!--[if lt IE 9]>
<script src="{{URL::to('/')}}/assets/global/plugins/respond.min.js"></script>
<script src="{{URL::to('/')}}/assets/global/plugins/excanvas.min.js"></script> 
<script src="{{URL::to('/')}}/assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
       
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{URL::to('/')}}/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{URL::to('/')}}/assets/pages/scripts/login-5.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        <script src="{{URL::to('/')}}/assets/apps/scripts/site.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script>
$(window).scroll(function(){
    if ($(window).scrollTop() >= 300) {
        $('.navbar-inverse').addClass('fixed-header');
    }
    else {
        $('.navbar-inverse').removeClass('fixed-header');
    }
});
</script>

<script>
function openNav() {
  document.getElementById("mySidebar").style.width = "350px";
  document.getElementById("main").style.marginLeft = "350px";
  document.getElementById("mySidebar").style.marginRight= "10px;";
}

function closeNav() {
  document.getElementById("mySidebar").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
  document.getElementById("mySidebar").style.marginRight= "-8px;";
}
</script>
    </body>

</html>