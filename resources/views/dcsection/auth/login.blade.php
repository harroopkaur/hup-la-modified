@extends('dcsection.layout.auth')

@section('content')
<html lang="{{ app()->getLocale() }}">
<head>
<title>Portal husting</title>

  <link rel="stylesheet" href="{{url('/assets/dcloginfiles/style.css')}}">
   <link rel="stylesheet" href="{{url('assets/landing/main.css')}}">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cutive+Mono|Lato:300,400">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
   <link rel="stylesheet" href="{{url('assets/dcloginfiles/app/styles/progress-tracker.css')}}">
   
       <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cutive+Mono|Lato:300,400">
<style>
.arroeser-pos951 {width: 89% !important;}
</style>
  </head>
<body>
  
  <div class="container">
    <div class="row">
<div class="col-md-12 col-sm-12 col-lg-12 main-headersty">
  <div class="col-md-4">
<img class="logo-topsty" src="{{url('assets/landing/img/logo.png')}}">
</div>
<div class="col-md-6">
<ul class="top_contlinks">
                        <li><a><i class="fa fa-phone"></i>(305)851-3545</a></li>
                        <li><a href="mailto:info@licensingassurance.com" title="info@licensingassurance.com"><i class="fa fa-envelope"></i>info@licensingassurance.com</a></li>
                      
            
          
                    </ul>
</div>
<div class="col-md-2 selctlang-sty">

<select class="lang-iconchange" name="forma" onchange="location = this.value;">
<option value="">Select Language</option>
<option value="{{ url('locale/en') }}">ENGLISH</option>
<option value="{{ url('locale/es') }}">SPANISH</option>
</select>
</div>
</div>
</div>
</div>


<!-- ===============================slider data start================== -->
<nav class="navbar navbar-inverse">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
     
    </div>
    <div class="collapse navbar-collapse center-datamenu" id="myNavbar">
      <ul class="nav navbar-nav uldatacenter-menuy951">
        <li><a href="{{url('')}}">Home</a></li>        
        <li><a href="#">Data Center</a></li>
        <li><a href="#">Knowledge Base</a></li>
        <li><a href="#">Contact</a></li>
        <li><a href="#" onclick="openNav()">Login</a></li>
      </ul>
     
    </div>
  </div>
</nav>
  
  <!-- ================================= -->
  <div id="mySidebar" class="sidebar">
<div class="pop-closesty951">
  <span class="closebtn" onclick="closeNav()">×</span>
</div>
<div class="login-maindatahad">
    <p>Login</p> 
</div>
<div class="main-popdataleft">
  <ul>
      <li><a href="{{url('dcsection/')}}" class="login-s">Log In DC</a></li>
      <li><a href="{{url('user/')}}" class="login-s">Log In End User</a></li>
      <li><a href="#" class="login-s">Log In DC</a></li>
  </ul>
</div>
</div>
<!-- ===================================== -->

 <div class="main-topbanner951">
  <div class="main-layer951data">
    <div class="container">
      <div class="row">
    <h6>DataCenter</h6>
  </div>
  </div>
  </div>
 </div>

<!-- =====================slider data close ======================== -->

<!-- ======================================== -->
<div class="main-stycolor951">
  <div class="container">
    <div class="row">

<!-- =============== sec part start=============================== -->
<div class="col-md-12 main-databg651">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
       <legend class="process-flowsty951">  {{ __('messages.total access') }} </legend>
    </div>
    <div class="col-md-7 col-sm-12 col-xs-12">
        
      <div class="col-md-12 col-sm-12 col-xs-12 main-martop951">
        <div class="col-md-6 col-sm-12 col-xs-12 end-userimgsty">
          <img src="{{url('assets/landing/img/end-userimg.png')}}">
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="end-userdata951sty">
               <button >{{ __('messages.endUsers') }}</button>
               <h4>{{ __('messages.access to end user app') }}</h4>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-sm-12 col-xs-12 main-martop951">
        <div class="col-md-6 col-sm-12 col-xs-12 end-userimgsty">
          <img src="{{url('assets/landing/img/data-centerimg.png')}}">
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
          <div class="data-userdata951sty">
               <button >{{ __('messages.datacenter') }}</button>
                <h4>{{ __('messages.vM Inventory') }}</h4>
          </div>
        </div>
      </div>
    </div>
<!-- ===================== form data start ============= -->
    <div class="col-md-5 col-sm-12 col-xs-12 moderan-dataulsty">
   <div class="form-maindatabg951">

    <div class="form-loginsty951">
 <h6> <!-- {{ __('messages.log In') }} -->  <i class="fa fa-user-plus" aria-hidden="true"></i>
 LogIn DC Portal
 </h6>
</div>

<div class="col-md-12 dcloginn">
  <center>

<form class="form-horizontal" role="form" method="POST" action="{{ url('/dcsection/login') }}">
                        {{ csrf_field() }}
     <div class="input-group login-forminputsty">
      <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
     <input id="email" type="email" class="form-control"placeholder="{{ __('messages.email') }}" name="email" value="{{ old('email') }}" autofocus>
    </div>
 @if ($errors->has('email'))
                                    <span class="">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif<br>
    <div class="input-group  login-forminputsty"> 
      <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
     <input id="password" type="password" class="form-control" placeholder="{{ __('messages.password') }}"name="password"> </div>

                                @if ($errors->has('password'))
                                    <span class="">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
<br>

<center><button class="login-dcsty"> {{ __('messages.log In') }}
</button></center>

<div class="create-newacountsty951">
  <a href="#">Create New Account</a>
</div>

</form>
</center>
</div>
<div style="clear: both;"></div>
</div>
    </div>
    <!-- ===================== form data close ============= -->
</div>
</div> 
</div>
</div>
</div>
<!-- =============== sec part  close =============================== --> 
<div style="background-color: #fcfcfc;">
 <div class="container">
    <div class="row">



<div class="col-md-12 col-sm-12  main-databg651">
  <div class="col-md-12 col-sm-12 ">
<h3 class="process-flowsty951">{{ __('messages.process Flow') }} {{ __('messages.datacenter') }}</h3>
</div>
<div class="col-md-12 col-sm-12 te_center ">
        <ul class="progress-tracker progress-tracker--text progress-tracker--text-top">
          <li class="progress-step is-complete">
            <span class="progress-text">
        <img src="{{url('/assets/landing/img/password.png')}}">
                  <h4 class="progress-title">{{ __('messages.log In') }}</h4>

            </span>
            <span class="progress-marker">1</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
          <img src="{{url('/assets/landing/img/shelf.png')}}">
              <h4 class="progress-title">{{ __('messages.vM Inventory') }}</h4>
          
            </span>
            <span class="progress-marker">2</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
      <img src="{{url('/assets/landing/img/add-friend.png ')}}">
              <h4 class="progress-title">{{ __('messages.endUser Management') }}</h4>
              
            </span>
            <span class="progress-marker">3</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
      <img src="{{url('/assets/landing/img/interview.png')}}">
                 <h4 class="progress-title">{{ __('messages.endUser Request') }}</h4>
           
            </span>
            <span class="progress-marker">4</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
      <img src="{{url('/assets/landing/img/stats.png')}}">
              <h4 class="progress-title">{{ __('messages.user Inventory') }}</h4>
              
            </span>
            <span class="progress-marker">5</span>
          </li>
        </ul>
  </div>  
</div>

<div class="arroeser-pos951">
<img src="{{url('assets/landing/img/arrow.png')}}">
</div>
</div>
</div>
</div>


<div class="">
 <div class="container">
    <div class="row">
<div class="col-md-12 col-sm-12 main-databg651">
 <div class="col-md-12 col-sm-12">
      <h3 class="process-flowsty951">{{ __('messages.process Flow End user') }}</h3></div>
      <div class="col-md-12 col-sm-12  te_center">
     <ul class="progress-tracker progress-tracker--text progress-tracker--text-top">
          <li class="progress-step is-complete">
            <span class="progress-text">
        <img src="{{url('/assets/landing/img/password.png')}}">
                  <h4 class="progress-title">{{ __('messages.log In') }}</h4>
             
            </span>
            <span class="progress-marker own_color ">1</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
          <img src="{{url('/assets/landing/img/file.png')}}">
              <h4 class="progress-title">{{ __('messages.assigned License') }}</h4>
          
            </span>
            <span class="progress-marker own_color " >2</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
      <img src="{{url('/assets/landing/img/literature.png')}}">
              <h4 class="progress-title">{{ __('messages.send to DC update info') }}</h4>
              
            </span>
            <span class="progress-marker own_color " >3</span>
          </li>
        </ul>
  </div>
</div>




</div>
</div>
</div>
<footer style="background-color: rgb(14, 94, 194);padding: 15px; width: 100%;">
  <div class="container">
  <div class="row" style="margin: 0; width: 100%">
    <div class="col-lg-3 col-md-12 dv1150_100619 logofooter-center">
      <div class="logo-footersty">
       <img class="img-footerlogo" src="{{url('assets/landing/img/footerlogo.png')}}">
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <p class="footer_heading1">Our Products</p>
      <ul class="footer_links">
        <li>
          <a class="footer-links" target="_blank" href="https://www.licensingassurance.com/SAMasaService.php">SAM as a Service</a>
        </li>
        <li>
          <a class="footer-links" target="_blank" href="https://www.licensingassurance.com/auditDefense.php">Audit Defense</a>
        </li>
        <li>
          <a class="footer-links" target="_blank" href="https://www.licensingassurance.com/SPLA.php">SAM as a Service for SPLA</a>
        </li>
        <li>
          <a class="footer-links" target="_blank" href="https://www.licensingassurance.com/deployment.php">Deployment Diagnostic</a>
        </li>
        <li>
          <a class="footer-links" target="_blank" href="https://www.licensingassurance.com/overview.php">Overview</a>
        </li>
      </ul>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <p class="footer_heading1">Our Team</p>
      <ul class="footer_links">
        <li>
          <a class="footer-links" target="_blank" href="https://www.licensingassurance.com/aboutUs.php">About Us</a>
        </li>
        <li>
          <a class="footer-links" target="_blank" href="https://www.licensingassurance.com/contact.php">Contact Us</a>
        </li>
        <li>
          <a class="footer-links" target="_blank" href="https://www.licensingassurance.com/articles.php">Article</a>
        </li>
        <li>
          <a class="footer-links" target="_blank" href="#">Videos</a>
        </li>
      </ul>
    </div>
    <div class="col-lg-3 col-md-12 col-sm-12 padding-leftzero">
      <p class="footer_heading1">Contact Us!!</p>

      <ul class="footer_txt">
        <li>
          <span>Phone: (305)851-3545</span>
        </li>
        <li>
          <span>info@licensingassurance.com</span>
        </li>
      </ul>

      <ul class="footer_txt">
        <li>
          <span>Licensing Assurance LLC</span>
        </li>
        <li>
          <span>16192 Coastal Highway Lewes, DE 19958</span>
        </li>
      </ul>

      <ul class="footer_txt">
        <li>
          <span>We are available</span>
        </li>
        <li class="pad_l_15">
          <span>Monday - Friday</span>
        </li>
        <li class="pad_l_15">
          <span>8:00 am - 5:00 pm</span>
        </li>
        <li class="pad_l_15">
          <span>East Time</span>
        </li>
      </ul>

    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 footer_copy_right">
      <p>Copyright Hosting Usage Portal. All Rights Reserved</p>
    </div>
  </div>
</div>
</footer>
<!-- <center>
<footer class="page-footer font-small blue pt-4 ">

  <div class="footer-copyright text-center py-3">© 2019 Copyright:
    <a href="https://mdbootstrap.com/education/bootstrap/"> HUP.com</a>
  </div>
  

</footer></center> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="{{url('assets/dcloginfiles/app/scripts/site.js')}}"></script>

<script>
$(window).scroll(function(){
    if ($(window).scrollTop() >= 300) {
        $('.navbar-inverse').addClass('fixed-header');
    }
    else {
        $('.navbar-inverse').removeClass('fixed-header');
    }
});
</script>

<script>
function openNav() {
  document.getElementById("mySidebar").style.width = "350px";
  document.getElementById("main").style.marginLeft = "350px";
  document.getElementById("mySidebar").style.marginRight= "10px;";
}

function closeNav() {
  document.getElementById("mySidebar").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
  document.getElementById("mySidebar").style.marginRight= "-8px;";
}
</script>
</body>
</html>
<style type="text/css">
  .btnbtn{
  color:#fff;
  font-size:15px;
  font-weight:600;
  background:#9f17f6;
  border-radius:10px;
  padding:2% 13% 2% 13%
}
</style>
@endsection