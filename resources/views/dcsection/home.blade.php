@extends('dcsection.includes.main')

@section('content')
<style type="text/css">
  .dcdc{
    background:#5a1187!important ;
    color:#fff;
    font-weight:600!important;
    font-size:20px!important;
    text-align:center!important;
  }
    .eueu{
    background:#a30082!important ;
    color:#fff;
      font-weight:600!important;
    font-size:20px!important;
    text-align:center!important;
  }
    .aa{
    background: #ed671e!important;
    color:#fff;
      font-weight:600!important;
    font-size:20px!important;
    text-align:center!important;
  }
  .alignth{
    vertical-align:middle!important;
  }
  td{
  	padding:4px!important;
  }
    th{
  	padding:4px!important;
  }
</style>
 
<div class="row">
<div class="col-md-4">
     
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                     
                      <div class="portlet-body">
                        <table class="table circle">

                                            <tr class="table table-striped table-responsive bg-nonesty">
               <form action="{{action('dcsectionController\dcsectionController@exportdata')}}"method="post">
                {{csrf_field()}}
                <input type="hidden" name="dcid"value="{{ Auth::user()->id}}"/>
                                       <td><a><button onclick="return confirm('{{ __('messages.you are Sure to download VTool') }}');"class="btn grey-salt circle bbb"type="submit"><i class="fas fa-download"></i>  {{ __('messages.download VTool') }}</button></a> </td>
                                     

                                               <td><a data-toggle="modal" data-target="#myModalcvs" class="editbtnh btn grey-salt circle  bbb"> <i class="fas fa-upload"></i>  {{ __('messages.load Result') }}</a>


                                               </td>
                                                    

                                                             <td><a href=""class="btn grey-salt circle  bbb"> <i class="fa fa-refresh bbcd"></i>
                                {{ __('messages.refresh') }}</a></td></form>  
                                <td>


<form action="{{action('dcsectionController\dcsectionController@clean')}}" method="post">
     {{csrf_field()}}
                                  <button  onclick="return confirm('Are You Sure  clean Data ')"type="submit" name="allclean"class="circle btn grey-salt "style="color:#fff;"><i class="fa fa-trash-o" aria-hidden="true"></i>
 Clean Data</button>
</form>
                                </td>

</tr>
</table>
</div>  
</div>
 <div class="col-md-6">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="">
                               @if(session()->has('success'))
   <span class="portlet box green text-white circle"style="padding:10px!important;color:#fff!important;"> {{session('success')}}  <i class="fa fa-check"></i></span>
    @endif
    @if($errors)
      <ul style="list-style-type:none;padding:0px;color:red;background:#fff">
     @foreach ($errors->all() as $error)

                <li style="padding:2px;"><b>{{ $error }}</b></li>
                  
                   
            @endforeach
                </ul>

@endif
                            </div>
                          
                        </div>
                      </div>
                            <!-- END EXAMPLE TABLE PORTLET-->


<!-- ======================new table data start============================ -->


<!-- ==============new table data close===================== -->
<div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light">
                               <div class="portlet-body">
                                
                                    <table class="table table-striped table-bordered table-hover table-header-fixed "style="" id="sample_1">


                                        <thead>
                                             <tr>
  <td colspan="8" class="dcdc">{{ __('messages.datacenter') }}</td>
   <td colspan="10"class="eueu">{{ __('messages.endUsers') }}</td>
    <td colspan="2" class="aa">{{ __('messages.action') }}</td>
</tr>
                                         <tr>

                                                <th class="alignth"> {{ __('messages.sr.No.') }}</th>
                                             <!--    <th class="text-center">send&nbspRequest</th> -->
                                                <th class="text-center alignth">End&nbspUser</th>
                                                <th class="text-center alignth"> {{ __('messages.update data') }}</th>
                                                <th class="text-center alignth"> {{ __('messages.vm') }}</th>
                                                   <th class="text-center alignth">{{ __('messages.host') }}</th>
                                                <th class="text-center alignth"> {{ __('messages.cores') }} </th>
                                                <th class="text-center alignth"> {{ __('messages.procs') }}</th>
                                                <th class="text-center alignth"> {{ __('messages.date install') }}</th>
                                                <th class="text-center alignth"> 
  <img  src="{{url('/assets/layouts/layout2/img/sql.png')}}" alt="sql"/><br>
                           {{ __('messages.sql') }}
                        </th>
                                                <th class="text-center alignth"> 
 <img  src="{{url('/assets/layouts/layout2/img/office.png')}}" alt="office"/><br>
                                                {{ __('messages.office') }}</th>
                                                <th class="text-center alignth">
 <img  src="{{url('/assets/layouts/layout2/img/sharepoint.png')}}" alt="logo"/><br>
                                                 {{ __('messages.sharepoint') }}</th>
                                                <th class="text-center alignth">
 <img  src="{{url('/assets/layouts/layout2/img/exchange.png')}}" alt="logo"/><br>
                                                 {{ __('messages.exchange') }} </th>
                                                <th class="text-center alignth">
 <img  src="{{url('/assets/layouts/layout2/img/skype.png')}}" alt="logo"/><br>
                                                {{ __('messages.skypebiz') }}</th>
                                                <th class="text-center alignth"> 
 <img  src="{{url('/assets/layouts/layout2/img/visualstudio.png')}}" alt="logo"/><br>
                                              {{ __('messages.visual Studio') }}</th>
                                                <th class="text-center alignth">
 <img  src="{{url('/assets/layouts/layout2/img/dynamic.png')}}"alt="logo"/><br>
                                                {{ __('messages.dynamic') }}</th>
                                                <th class="text-center alignth"> 
 <img  src="{{url('/assets/layouts/layout2/img/project.png')}}" alt="logo"/><br>
                                                {{ __('messages.project') }}</th>
                                                <th class="text-center alignth">
 <img  src="{{url('/assets/layouts/layout2/img/rds.png')}}" alt="logo"/><br>
                                                {{ __('messages.rds') }} </th>
                                                <th class="text-center alignth">
 <img  src="{{url('/assets/layouts/layout2/img/vision.png')}}" alt="logo"/><br>
                                                 {{ __('messages.visio') }}</th>

                                                <th class="text-center alignth"colspan=""> {{ __('messages.update') }} </th>
                                              <th class="text-center alignth"colspan="">  {{ __('messages.add New VM') }} </th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        <!-- error -->
                                        @php
                                        $i=1;
                                        @endphp
                                        <!-- end error -->

                                                  @foreach($vmdata as $key => $udata)  


                                            <tr>
 @if($udata->name =='')
<form action="{{action('dcsectionController\dcsectionController@updatevmuser')}}"method="post">
{{csrf_field()}}
@endif
                          <td class="text-center alignth">{{$i++}}</td>
                <!--           <td>&nbsp&nbsp&nbsp&nbsp<i class="fas fa-paper-plane"></i></td> -->
                          <td class="text-center alignth" rowspan=""> @if($udata->name)
                            <?php
                            $vmdatppa = explode(',', $udata['name']);
                            ?>  
                            {{$vmdatppa[0]}}
                            @else 

                            <input type="hidden" name="id"value="{{$udata->id}}">
                            
                            <select name="username"class="form-control btn"required>
                              <option value="">Select&nbspEU</option>
                              @foreach($name as $nam)
                              
                              <option value="{{$nam->name}},{{$nam->id}}">{{$nam->name}}</option>
                             @endforeach
                            </select>
                            @endif
                          </td>
                          <td class="text-center alignth">{{date('d-M-Y',strtotime($udata->updatedate))}}</td>
                    
                          <td class="text-center alignth">{{$udata->vm}}</td>
                          <td class="text-center alignth">{{$udata->host}}</td>
                          <td class="text-center alignth">{{$udata->cores}}</td>
                          <td class="text-center alignth">{{$udata->procs}}</td>
                          <td class="text-center alignth">{{date('d-M-Y',strtotime($udata->dateinstall))}}</td>
                          <td class="text-center alignth"><select class="btn disabled"><option>@if($udata->sql) {{$udata->sql}} @else Select @endif</option></select></td>
                          <td class="text-center alignth"><select class="btn disabled"><option>@if($udata->office) {{$udata->office}} @else Select @endif</option></select></td>
                          <td class="text-center alignth"><select class="btn disabled"><option>@if($udata->sharepoint) {{$udata->sharepoint}} @else Select @endif</option></select></td>
                          <td class="text-center alignth"><select class="btn disabled"><option>@if($udata->exchange) {{$udata->exchange}} @else Select @endif</option></select></td>
                          <td class="text-center alignth"><select class="btn disabled"><option>@if($udata->skypebiz) {{$udata->skypebiz}} @else Select @endif</option></select></td>
                          <td class="text-center alignth"><select class="btn disabled"><option>@if($udata->visualstudio) {{$udata->visualstudio}} @else Select @endif</option></select></td>
                          <td class="text-center alignth"><select class="btn disabled"><option>@if($udata->dynamics) {{$udata->dynamics}} @else Select @endif</option></select></td>
                          <td class="text-center alignth"><select class="btn disabled"><option>@if($udata->project) {{$udata->project}} @else Select @endif</option></select></td>
                          <td class="text-center alignth"><select class="btn disabled"><option>@if($udata->rds) {{$udata->rds}} @else Select @endif</option></select></td>
                          <td class="text-center alignth"><select class="btn disabled"><option>@if($udata->visio) {{$udata->visio}} @else Select @endif</option></select></td>
                            <td class="text-center alignth">
                              <!--  -->
                              <!-- model vm add -->
                              @if($udata->name =='')

<input type="submit" class="btn btn-primary"name="submit"value="{{ __('messages.assign End User') }}"/>
   </form> 
                              @else 
 <button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal134{{$udata->id}}">{{ __('messages.edit VM') }}</button>
@endif

  <!-- Modal -->

  <div class="modal fade" id="myModal134{{$udata->id}}" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <form action="{{url('/dcsection/updatevm')}}"method="post">
        {{csrf_field()}}
      <div class="modal-content"style="margin-top:150px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><center><b>{{ __('messages.edit VM Form') }}</b></center></h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="uid"value="{{$udata->id}}">
       <label>{{ __('messages.name') }}</label>
       <input type="text" name="name"class="form-control "value="{{$udata->name}}"disabled>
            <input type="hidden" name="name"class="form-control "value="{{$udata->name}}">
               <input type="hidden" name="user_id"class="form-control "value="{{$udata->user_id}}">
            <label>{{ __('messages.add VM') }} </label>
       <input type="text" name="vm"class="form-control"value="{{$udata->vm}}">
          <label>{{ __('messages.add Host') }} </label>
       <input type="text" name="host"class="form-control"value="{{$udata->host}}">
            <label>{{ __('messages.cores') }}</label>
       <input type="number" name="cores"class="form-control"value="{{$udata->cores}}">
            <label>{{ __('messages.procs') }}</label>
       <input type="number" name="procs"class="form-control"value="{{$udata->procs}}">
            <label>{{ __('messages.date install') }}</label>
       <input type="date" name="dateinstall"class="form-control"value="{{$udata->Date_Install}}">
     
       <input type="hidden" value="{{$udata->sql}}"name="sql">
     
       <input type="hidden" value="{{$udata->office}}"name="office">
    
       <input type="hidden" value="{{$udata->sharepoint}}"name="sharepoint">
    
       <input type="hidden" value="{{$udata->exchange}}"name="exchange">
  
       <input type="hidden" value="{{$udata->skypebiz}}"name="skypebiz">


       <input type="hidden" value="{{$udata->visualstudio}}"name="visualstudio">

       <input type="hidden" value="{{$udata->dynamics}}"name="dynamics">

       <input type="hidden" value="{{$udata->project}}"name="project">
   
       <input type="hidden" value="{{$udata->rds}}"name="rds">
     
       <input type="hidden"value="{{$udata->vision}}"name="vision">
       <br>
       <input type="submit" name="submit"class="btn btn-info">
        </div>
      </div>
    
    </div>
  </div>
  
</div>
</form>
                              <!--  -->
                            </td>
                          <td class="text-center alignth" rowspan="">
               
<!-- model vm add -->
@if($udata->name =='')
&nbsp
@else
 <button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal{{$udata->id}}">{{ __('messages.add New VM') }}</button> 
  <!-- Modal -->

  <div class="modal fade" id="myModal{{$udata->id}}" role="dialog">

    <div class="modal-dialog">

      <!-- Modal content-->
      <form action="{{url('/dcsection/savevm')}}"method="post">
        {{csrf_field()}}

      <div class="modal-content">
        <div class="modal-header">
         
                        
             <input type="hidden" name="uid"value="{{$udata->user_id}}">
        
          <button type="button" class="close" data-dismiss="modal">&times;</button>
 <h4 class="modal-title"><center><b>{{ __('messages.add VM Form') }}</br></center></h4>
        </div>
        <div class="modal-body">
       
       <label>{{ __('messages.name') }}</label>
      
       <input type="text" name="name"class="form-control "value="{{$vmdatppa[0]}}"disabled>
            <input type="hidden" name="name"class="form-control "value="{{$vmdatppa[0]}}">
   
            <label>{{ __('messages.add VM') }} </label>
       <input type="text" name="vm"class="form-control">
       <label>{{ __('messages.add Host') }}</label>
       <input type="text" name="host"class="form-control">
            <label>{{ __('messages.cores') }}</label>
       <input type="number" name="cores"class="form-control">
            <label>{{ __('messages.procs') }}</label>
       <input type="number" name="procs"class="form-control">
            <label>{{ __('messages.date install') }}</label>
       <input type="date" name="dateinstall"class="form-control">
       <br>
       <input type="submit" name="submit"class="btn btn-info">
        </div>
      </div>
    
    </div>
  </div>
  
</div>
</form>
<!-- model vm add -->
@endif
                          </td>
                        
                                             
                                            </tr>
                                            @endforeach
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                          </div>
                          <br><br><br><br><br><br>
                          <!-- <div class="row">
                            <form action="{{url('dcsection/sendemail')}}"method="post">
                              {{csrf_field()}}
                              <input type="text" name="email">
                                     <input type="text" name="subject">
                                            <textarea type="text" name="massage"></textarea>
                                                   <input type="submit"class="btn btn-primary" name="submit">
                             
                            </form>
                          </div> -->


 <!-- Modal -->

  <div class="modal fade" id="myModalcvs" role="dialog">
    <div class="modal-dialog ">
    
      <!-- Modal content-->
  
      <div class="modal-content  circle mttop"><br><br>
<center><h3 class="text-info"><b>{{ __('messages.load Result') }}</b></h3></center>
<hr/>
        <form action="{{ url('/dcsection/import')}}"method="post" enctype="multipart/form-data" >
    {{csrf_field()}}
 <center>
                                            <input type="file" name="file" multiple=""class=""required> </center><hr/><br>
     <center>  <input type="submit"class="btn btn-info circle"value="{{ __('messages.upload Result') }} " >&nbsp&nbsp&nbsp <button  type="button" class="btn btn-secondary circle" data-dismiss="modal">{{ __('messages.cancel') }}</button></center>
    </form>
    <br><br><br><br>
      </div>
    
    </div>
  </div>
  
</div>
</form>
<!-- model vm add -->






            

<!-- edc file excel -->
@endsection