@include('dcsection.includes.header')
@include('dcsection.includes.sidebar')

<div class="page-content-wrapper">
                <div class="page-content" style="overflow:hidden;">
	@yield('content')
	</div>
</div>
@include('dcsection.includes.footer')