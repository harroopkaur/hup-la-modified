<!DOCTYPE html>
<html lang ="{{ app()->getLocale() }}">
<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
  <style>
   .error{ color:red; } 
  </style>
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <!-- model notification -->

<script type="text/javascript">
    setInterval(function(){
    $('').text();

}, 2000);
</script>

<!-- start -->
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
        <meta charset="utf-8" />
        <title>HUP</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #2 for statistics, charts, recent events and reports" name="description" />
        <meta content="" name="author" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
           <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<!-- file upload -->
 <link href="{{url('/assets/global/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('/assets/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('/assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('/assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css')}}" rel="stylesheet" type="text/css" />
<!--  -->
      <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{url('/assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('/assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{url('/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{url('/assets/global/css/components.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{url('/assets/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{url('/assets/layouts/layout2/css/layout.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('/assets/layouts/layout2/css/themes/blue.min.css')}}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{url('/assets/layouts/layout2/css/custom.min.css')}}" rel="stylesheet" type="text/css" />

  
        <link rel="shortcut icon" href="favicon.ico" /> 
  <style type="text/css">
            .page-header.navbar .top-menu .navbar-nav > li.dropdown-extended .abcdef {
     min-width: 406px!important;
    max-width: 282px!important;
}
}
        </style>

    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo ">
                    <a href="index.html">
                        <img id="logo123" src="{{url('/assets/layouts/layout2/img/ff.png')}}" alt="logo" class="logo-default"/> </a>
                    <div class="menu-toggler sidebar-toggler">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN PAGE ACTIONS -->
                <!-- DOC: Remove "hide" class to enable the page header actions -->
                <div class="page-actions">
                    <div class="btn-group">
                        
                    </div>
                </div>
                <!-- END PAGE ACTIONS -->
                <!-- BEGIN PAGE TOP -->
                <div class="page-top">
                    <!-- BEGIN HEADER SEARCH BOX -->
                    <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
                    
                    <!-- END HEADER SEARCH BOX -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>


                            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                @if($count)
                                    <span id="test"class="badge badge-default">

  {{$count}}

                                   </span>
                          @else
                                    <span id="test22"class="badge badge-default vvvvvvvvvv"> </span>
                                          @endif
                                </a>


                                <ul class="dropdown-menu abcdef">
                                    <li class="external">
                                        <h3>

                                            @if($count){{$count}}
                                            @else
                                            <span class="bold vvvvvvvvvv">  </span> @endif
                                        New   notifications&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</h3>
                                
                                    </li>
                                    <li>
                                    
<ul class="dropdown-menu-list scroller showdata" style="height: 250px;" data-handle-color="#637283">
                               
                                  @foreach($notidataheader as $notidataa)
                                            <li style="border-bottom:1px solid black;">
                                                <a class="btn " href="{{url('dcsection/notification', $notidataa->id) }}">
                                                  <!--   <span class="time">                                                                                   
{{date(' h:i:s Y/M/d ',strtotime($notidataa->created_at))}}
                                                    </span> -->
                                                    
                                                    <span class="text-left">
                                                        <span class="label label-sm label-icon bold label-success text-left ">
                                                            <i class="fa fa-plus"></i> &nbsp {{$notidataa->nfrom}}  &nbsp:-&nbsp {{$notidataa->name}} Update  &nbsp&nbsp
                                                        </span> 


{{$notidataa->massage}}
  

                                                     
                                                       </span>
                                                </a>
 </li>
                                           @endforeach
                                        
                                        </ul>
                                    
                            

                                    </li>
                                </ul>
                            </li>





     

   <!--  <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                  <i class="icon-logout"></i>
                                               
                                   
                                </a></li> -->

                            <!-- END NOTIFICATION DROPDOWN -->
                            <!-- BEGIN INBOX DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            
                            <!-- END INBOX DROPDOWN -->
                            <!-- BEGIN TODO DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                     <i class="icon-user"></i></a>
                                    <ul class="dropdown-menu dropdown-menu-default">
                            <li>    
                              <a href="{{ url('/dcsection/logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="icon-key"></i> Log Out
                            </a>
                             <form id="logout-form" action="{{ url('/dcsection/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                            </li>
                            
                            
                </ul>
                                   </li>

                            <!-- dfdgdsgdfsgs -->
                           <!--  <li class="dropdown dropdown-extended "style="margin-top:5px!important;" id="header_notification_bar">
                                <a href="" >
                                  <i class="fa fa-envelope"></i></a>
                                
                                	<ul class="dropdown-menu dropdown-menu-default">
							<li>	
								<a href="{{ url('/dcsection/logout') }}"
								onclick="event.preventDefault();
								document.getElementById('logout-form').submit();">
								<i class="icon-key"></i> Log Out
							</a>
						 <form id="logout-form" action="{{ url('/dcsection/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
						</li>
				</ul>
                                </li> -->
                                
                                <li class="dropdown  dropdown-notification" id="header_notification_bar">
                                <a href="{{url('/dcsection/lanoti')}}" class="dropdown-toggle">
                                   <i class="fa fa-envelope"></i> </a>
                            
                                   </li>
                             
			 <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                   <i class="icon-globe"></i> </a>
                                    <ul class="dropdown-menu dropdown-menu-default">
                            <li>    
                                <a href="{{ url('locale/en') }}"
                                onchange="location = this.value;">
                                ENGLISH
                            </a>
                            </li>
                            <li>    
                                <a href="{{ url('locale/es') }}"
                                onchange="location = this.value;">
                                SPANISH
                            </a>
                            </li>
                            
                </ul>
                                   </li>
                                  
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-extended quick-sidebar-toggler">
                                <span class="sr-only">Toggle Quick Sidebar</span>
                                
                            </li>
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>

        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
         <div class="clearfix"> </div>
                            
                <style>
             .page-header.navbar .page-logo .logo-default {

margin: 6px 0 0 !important;

}
#logo123 {

height: 50px;

}
.page-header.navbar .top-menu .navbar-nav > li.dropdown-extended .dropdown-menu{
    min-width: 142px;

max-width: 150px;
}
.page-header.navbar .top-menu .navbar-nav > li.dropdown > .dropdown-toggle > i {
    font-size: 19px;
}
             </style>                   
                            
   <script type="text/javascript">
function loadlink(){

   $.ajax({
type:'POST',
url:'../ajaxRequest',
data:{id:'sdsadsadasdds',_token: '{!! csrf_token() !!}'},
success:function(data){
$('.vvvvvvvvvv').html(data.success);

$('.showdata').html(data.notiaa);
}

        });  
}

loadlink(); // This will run on page load
setInterval(function(){
    loadlink() // this will run after every 1 seconds
}, 8000);



    $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }
    });
</script>                         
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                         