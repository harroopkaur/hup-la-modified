@extends('dcsection.includes.main')

@section('content')

<style>
	.bg{
		background:#fff!important;
	}
</style>
<div class="container">
	<div class="col-md-12 bg" >
		<center><h2> <b>Data Center Template For End User </b></h2></center><hr/>
		<table class="table text-center table-striped table-bordered table-hover table-condensed">
			<tr>
				<th class="text-center">Sr.No.</th>
				<th class="text-center">name</th>
				<th class="text-center">Action</th>

			</tr>
			@php
			$sr = 1;
			@endphp
			@foreach($tmps as $tmp)
			<tr>
				<td>{{$sr++}}</td>
				<td>{{$tmp->name}}</td>
				<td><a href="{{url('dcsection/edittmp',$tmp->id)}}"class="btn btn-danger">Edit Template</a></td>

			</tr>
			@endforeach
		</table>

	</div>
</div>

@endsection