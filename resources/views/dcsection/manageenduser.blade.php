@extends('dcsection.includes.main')

@section('content')  
<style type="text/css">
  .bbcd{
    color:#2a19aa;

  }
  .bbb{
    color:#000!important;
  }
</style>
<div class="col-md-12">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                       <script type="text/javascript">
                           $(document).ready(function(){
                            $('.dt-buttons').hide();
                           });
                       </script>
                                <div class="portlet-body">

                                            <tr class="table table-striped table-responsive">
 <form action="{{url('/dcsection/action')}}"method="post">
    {{csrf_field()}}
                                        <td><a data-toggle="modal" data-target="#modalLoginForm" class="btn grey-salt circle bbb"> <i class="fas fa-user-plus bbcd"></i>{{ __('messages.add EU') }}</a></td>

                                               <td><a data-toggle="modal" data-target="#sss" class="editbtnh btn grey-salt circle btndisable bbb"> <i class="fas fa-user-edit bbcd"></i>{{ __('messages.edit EU') }}</a>
                                               </td>
                                                     <!-- active/inactive -->
<div class="modal fade " id="sssss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content circle"style="margin-top:200px">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold text-info  actinacheading"style="font-size:26px"><b> </b></h4>
     
      </div>
      <div class="modal-body ">
<center><h4 class="bold">Are you sure To <span class="actinacheading"></span> User</h4></center><br>
<center>
<tr><td><input type="checkbox"name="sendemail"value="sendmail"/><span>&nbspNotify User by E-mail</span><td></tr></center><br></br>
<table class="table text-center">
<tr>
  <td><span class="actinact">
  </span>

</td>
<td><input data-dismiss="modal" aria-label="Close" type="button" class="btn grey-salt  pl-5 pr-5"value="No"> </td>
<tr></table>
  </div>
</div></div></div>
  <!-- end inactive -->
   <td><a data-toggle="modal" data-target="#sssss" class="editbtnh btn grey-salt circle btndisable"> 
    &nbsp<i class="fas fa-user-times"></i>     Inactive/Active User </a></td>

                                                

                                                             <td><a data-toggle="modal" data-target="#mailmodel" class="btn grey-salt circle btndisable mailbtn"> <i class="fa fa-envelope"></i> {{ __('messages.preview Mail') }}</a></td>

<!-- delete -->
<div class="modal fade " id="ssss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content circle"style="margin-top:200px">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold text-info"style="font-size:26px"><b>Delete</b></h4>
     
      </div>
      <div class="modal-body ">
<center><h4 class="bold">Are you sure To Delete User</h4></center><br><center>
<tr><td><input type="checkbox"name="sendemail"value="sendmail"/><span>&nbspNotify User by E-mail</span><td></tr></center><br></br>
<table class="table text-center">
<tr>
  <td>
<button  name="delete"type="submit" class="btn grey-salt  btndisable pl-5 pr-5">Yes </button>
</td>
<td><input data-dismiss="modal" aria-label="Close" type="button" class="btn grey-salt  pl-5 pr-5"value="No"> </td>
<tr></table>
  </div>
</div></div></div>
<!-- end delete -->
    <td><a data-toggle="modal" data-target="#ssss" class="editbtnh btn grey-salt circle btndisable"> <i class="fas fa-user-edit "></i>
    Delete DC </a></td>
<td >
    @if(session()->has('success'))
   <span class="portlet box green text-white circle bbb"style="padding:10px!important;color:#fff!important;"> {{session('success')}}  <i class="fa fa-check bbcd"></i></span>
    @endif
          </ul>
@if($errors)
      <ul style="list-style-type:none;padding:0px;color:red;background:#fff">
     @foreach ($errors->all() as $error)

                <li style="padding:2px;"><b>{{ $error }}</b></li>
                  
                   
            @endforeach
                </ul>
@endif
</td>
</tr><hr/>
                                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
<thead>
<tr>
 <th></th>
<th>Sr.No.</th>
 <th>{{ __('messages.select') }}</th>
<th>{{ __('messages.user') }}</th>
<th>{{ __('messages.email') }}</th>
<th>{{ __('messages.password') }}</th>
<th>{{ __('messages.type Contract') }}</th>
<th>{{ __('messages.date Contract') }}  </th>
<th>{{ __('messages.comment') }}</th>
<th>Status</th>

    </tr>
</thead>
    <tbody>
@php
$ii = 1;
@endphp
        @if($data)

        @foreach($data as $key => $datas)
    <tr>
        <td></td>
<td>{{$ii++}}</td>
<td><input type="checkbox"class="one" name="action[]"id="toggle" value="{{$datas->id}}">
</td>
<td>{{$datas->name}}</td>
<td>{{$datas->email}}</td>
<td><!-- passwoord -->
  @php 
  $str = $datas->password ;
 $leg =  strlen($str);
  @endphp  
<?php 
for ($i=1; $i <= 6; $i++) { 
 echo 'x';
}
?></td>
<td>{{$datas->typecontract}}</td>
<td>{{$datas->datecontract}}</td>
<td>{{$datas->comment}}</td>
 <td><center>@if($datas->status == '1')
<a class="btn btn-sm btn-success disabled">Active</a>
@else
<a class="btn btn-sm btn-danger disabled">Inactive</a>
@endif

  <center></td>
    </tr>

@endforeach

@endif
                                        </tbody>
                                    </table>
                                </form>

                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>


<!-- end model -->
  <!-- mail model -->
<div class="modal fade " id="mailmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content circle">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold text-info"style="font-size:26px"><b>{{ __('messages.send Mail') }}</b></h4>
     
      </div>
      <div class="modal-body ">
        <!-- mail form -->
 <form method="post" action="{{action('dcsectionController\dcsectionController@sendemail')}}">

    <div class="row">
      <img src="{{url('/img/mail.png')}}"width="100%">
    </div>
    {{csrf_field()}}
<div class="sendmail"style="margin-bottom:0px">
 


    </div>

      </form>
  </div>
</div></div></div>
                    <!-- end edit model -->
                    <!-- end mail model -->
                    <!-- edit model -->
<div class="modal fade " id="sss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content circle">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold text-info"style="font-size:26px"><b>{{ __('messages.edit EU') }}</b></h4>
     
      </div>
      <div class="modal-body ">
        <!-- dc form -->
 <form method="post" action="{{url('/dcsection/update')}}">
{{csrf_field()}}
<div class="editdcform"style="margin-bottom:8px"id="editdcform">

    </div>
      </form>
  </div>
</div></div></div>
                    <!-- end edit model -->
                    <!-- f---------------------------------------------------edit end dc -->
                    <!-- model dc form -->
                    <style type="text/css">
                        .sendmail{
  text-decoration:none!important;
}
                    </style>
                    <!--add dc model  -->
<div class="modal fade " id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content circle">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold text-info"style="font-size:26px"><b>{{ __('messages.add EU') }}</b></h4>
     
      </div>
      <div class="modal-body ">
        <!-- dc form -->
 <form method="post" action="{{url('/dcsection/save')}}">
{{csrf_field()}}
<input type="hidden" name="dcid"value="{{ Auth::user()->id}} ">
<div class="input-group"style="margin-bottom:8px">
<span class="input-group-addon">
<i class="fa fa-user"></i>
</span>
<input type="text" class="form-control" placeholder="{{ __('messages.enter username') }}"name="username"> </div>
   
   <div class="input-group"style="margin-bottom:8px">
<span class="input-group-addon">
<i class="fa fa-lock fa-fw"></i>
</span>
<input type="password" class="form-control" placeholder="{{ __('messages.enter password') }}"name="password"> </div>  

<div class="input-group"style="margin-bottom:8px">
<span class="input-group-addon">
<i class="fa fa-envelope"></i>
</span>
<input type="email" class="form-control" placeholder="{{ __('messages.email address') }}"name="email"> </div>
<div class="input-group"style="margin-bottom:8px">
<span class="input-group-addon">
<i class=" icon-layers d"></i>
</span>
<input type="text" class="form-control" placeholder="{{ __('messages.enter type contract') }} name"name="typecontract"> </div>
<div class="input-group"style="margin-bottom:8px">
<span class="input-group-addon">
<i class="icon-calendar"></i>
</span>
<input type="date" class="form-control" placeholder="{{ __('messages.dd/mm/yyyy') }}"name="datecontract"> </div>

<div class="input-group"style="margin-bottom:8px">
<span class="input-group-addon">
<i class="fa fa-sticky-note-o"></i>
</span>
<textarea type="text" class="form-control" placeholder="{{ __('messages.comment') }}"name="comment"></textarea> </div>
<tr>

<td><input type="checkbox" class="" value="sendmail"name="sendemail"> Notify EU by E-mail</td>
<td></td>
</tr><br><br>
<tr>
    <td>
<input type="submit" class="btn btn-info circle"value="{{ __('messages.submit') }}"> &nbsp&nbsp</td>
<td>
<input data-dismiss="modal" aria-label="Close" type="button" class="btn grey-salt circle"value="{{ __('messages.cancel') }}"> 
</td>
</tr>
    </form>
<!-- end dc form -->
    </div>
</div>
  </div>
</div>

@endsection 