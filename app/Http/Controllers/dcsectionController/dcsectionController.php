<?php

namespace App\Http\Controllers\dcsectionController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use view;
use App\enduser;
use App\User;
use validator;
use App\Vminventory;
use DateTime;
use DB;
use Excel;
use Mail;
use Auth;
use Importer;
use App\notifications;
use App\dctemp;
use  App\tmp;

class dcsectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function __construct()
    {
         $this->middleware('dccheckstatus'); 
        $this->middleware('dcsection');  
    }


public function tmpupdate(Request $request){


$idt = $request->idt;

$enduser['tmps']= DB::table('dctemps')->where('id',$idt)->update([

'tmp'=>$request['tmp'],
]);
  return redirect('/dcsection/tmpmanage');
}


// Public  function noti(){

// $idddd = Auth::user()->id;
// $count['count'] = notifications::where('dcid',$idddd)->where('status',0)->count();

//   return view('dcsection.includes.header',$count);
// }

 public function edittmp($id){
 $idddd = Auth::user()->id;
   $enduser['count']= notifications::where('dcid',$idddd)->where('status',0)->count();
  $enduser['notidataheader']= notifications::where('dcid',$idddd)->where('status',0)->where('parent','qns')->get();

$enduser['tmps']= DB::table('dctemps')->where('dcid',$idddd)->where('id',$id)->get();


      return view('dcsection.edittemp',$enduser);
    }
public function templates(){
  $idddd = Auth::user()->id;
  $enduser['count']= notifications::where('dcid',$idddd)->where('status',0)->count();
  $enduser['notidataheader']= notifications::where('dcid',$idddd)->where('status',0)->where('parent','qns')->get();

    $enduser['tmps']= DB::table('dctemps')->where('dcid',$idddd)->get();

  return view('dcsection.dctemp',$enduser);
}

    public function enduserdata()
    {
$idddd = Auth::user()->id;
$enduser['data'] = enduser::where('dc_id',$idddd)->get();
$idddd = Auth::user()->id;
$enduser['count']= notifications::where('dcid',$idddd)->where('status',0)->count();
$enduser['notidataheader']= notifications::where('dcid',$idddd)->where('status',0)->where('parent','qns')->get();
   return view('dcsection.manageenduser', $enduser);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
public function sendmassage(Request $request,$id){

notifications::create([
'name'=>Auth::user()->name,
'dcid'=>Auth::user()->id,
'userid'=>$request['userid'],
'massage'=>$request['massage'],
'nfrom'=>'dc',
'nto'=>'user',
'parent'=>$id,
'status'=>'3',
]);
notifications::where('id',$id)->update([
                          
                          'status'=>'3',
                          ]);
return redirect()->to('dcsection/notification/'.$id);
}


public function notiview($id){

notifications::where('id',$id)->where('status','0')->update([
  'status'=>'1',
]);
notifications::where('parent',$id)->where('status','0')->update([
  'status'=>'1',
]);
// 
$idddd = Auth::user()->id;
$vmdata['count']= notifications::where('dcid',$idddd)->where('status',0)->count();

$vmdata['notidata']= notifications::where('dcid',$idddd)->where('id',$id)->get();

$vmdata['notidataheader']= notifications::where('dcid',$idddd)->where('status',0)->where('parent','qns')->get();
$vmdata['distinct']   = Vminventory::where('dc_id',$idddd)->select('user_id')->get();
$vmdata['name'] =  User::select('name','id')->get();
$datel  = date('Y-m-d H:i:s');
$vmdata['date'] =   date('Y-m-d H:i:s',strtotime('+2 minutes',strtotime($datel)));



$vmdata['chatparent']= notifications::where('id',$id)->get();
$vmdata['conversation']= notifications::where('parent',$id)->get();

   return view('dcsection.noti',$vmdata);

  
}

    // 

     // 
  public function sendemail(Request $request)
{
  // dd($request);
$email = $request->email;
$password = $request->password;
$cname = $request->cname;
$cdate = $request->cdate;
$comment = $request->comment;
$user = $request->name;

      Mail::send('dcsection.emails.dcmail', ['user' => $request->name,'password' => $request->password,'cname' => $request->cname,'email' => $request->email,'cdate' => $request->cdate,'comment' => $request->comment], function ($m) use ($request) {
$m->from('virenderminhas22@gmail.com', 'LA HUP');

$m->to($request->email, 'test')->subject('Data Center Login');

});

return redirect('/dcsection/EnduserManagement')->with('success','Mail  Send Successfully  ');

   }

    // /
    // 
public function updatevmuser(Request $request){


        $request->validate([
'username'=>'required',
]);
 $nameid = explode(',', $request['username']);
 // dd($nameid);
vminventory::where('id',$request->id)->update([
'name'=>$nameid[0],
'user_id'=>$nameid[1],

]);


  return redirect('/dcsection/home')->with('success','Successfully Assign EU');
}




    public function updatevm(Request $request)
    {
                   $request->validate([
  'name'=>'required',
  'uid'=>'required',
  'procs'=>'required',
  'vm'=>'required',
  'cores'=>'required',
  'dateinstall'=>'required',
      ]);
            if($request['sql']==''){
$sql='';
            }else{
           $sql=$request['sql'];   
            }

             if($request['office']==''){
$office='';
            }else{
              $office = $request['office'];
            }
             if($request['sharepoint']==''){           
$sharepoint='';
            }else{

  $sharepoint = $request['sharepoint'];      
            }
             if($request['exchange']==''){
$exchange='';
            }else{
            $exchange=$request['exchange'];  
            }


             if($request['skypebiz']==''){
$skypebiz='';
            }else{
          $skypebiz=$request['skypebiz'];    
            }
             if($request['visualstudio']==''){
            
$visualstudio='';
            }else{

$visualstudio=$request['visualstudio'];    
            }
             if($request['dynamics']==''){
            
$dynamics='';
            }else{
              $dynamics=$request['dynamics'];
            }
             if($request['project']==''){
            
$project='';
            }else{

$project=$request['project'];  
            }
             if($request['rds']==''){
            
$rds='';
            }else{
$rds=$request['rds'];
            }
             if($request['visio']==''){
            
$visio='';
            }else{
              $visio=$request['visio'];
            }
         $id =  $request['uid'];
$now = new DateTime();
Vminventory::where('id',$id)->update([
'name'=>$request['name'],
'user_id'=>$request['user_id'],
'dc_id'=>Auth::user()->id,
'host'=>'',
'endUser'=>$request['name'],
'updatedate'=>$now,
'vm'=>$request['vm'],
'cores'=>$request['cores'],
'procs'=>$request['procs'],
'dateinstall'=>$request['dateinstall'],
'sql'=>$sql,
'office'=>$office,
'sharepoint'=>$sharepoint,
'exchange'=>$exchange,
'skypebiz'=>$skypebiz,
'visualstudio'=>$visualstudio,
'dynamics'=>$dynamics,
'project'=>$project,
'rds'=>$rds,
'visio'=>$visio,
  ]);

        return redirect('dcsection/home')->with('success','successfully Update VM ');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

      public function store(Request $request)
    {  
      $dcid = Auth::user()->id;
    request()->validate([
         'dcid' => 'required',
        'username' => 'required|unique:endusers,name',
        'email' => 'required|email|unique:endusers',
        'typecontract' => 'required',
        'datecontract' => 'required',
        'comment' => 'required',
          'password' => 'required',
        ]);

        $data = $request->all();

         $user = User::create([
'name'=>$data['username'],
'email'=>$data['email'],
'status'=>'1',
'password'=>bcrypt($data['password']),
        ]);
$iduser = $user->id;
// dd($iduser);
        $check = enduser::create([
   'id'=> $iduser ,   	
'name'=>$data['username'],
'dc_id'=>$data['dcid'],
'email'=>$data['email'],
'typecontract'=>$data['typecontract'],
'datecontract'=>$data['datecontract'],
'comment'=>$data['comment'],
'password'=>bcrypt($data['password']),
'status'=>'1',
        ]);


if(!empty($request->sendemail)){

$email = $request->email;
$password = $request->password;
$company = $request->datecontract;
$user = $request->username;
$data1 = DB::table('dctemps')->where('name','addeu')->where('dcid',$dcid)->get();
// dd($data1[0]['tmp']);
Mail::send('dcsection.emails.dcmail', ['user' => $user,'password' => $password,'datecontract' => $request->datecontract,'email' => $request->email,'Template'=>$data1[0]->tmp], function ($m) use ($email,$request) {
$m->from('virenderminhas22@gmail.com', 'LA HUP');

$m->to($email, 'test')->subject('HUP ');

});
}
        return redirect('dcsection/EnduserManagement')->with('success','EU successfully Created ');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 public function action(Request $request)
    {

      // dd($request['action']['0']);
    $email=   enduser::select('email')->where('id',$request['action'][0])->get();

     // $pass['pass'] =   endUser::select('password')->where('id',$request['action'])->get();
 // dd($email[0]->email);
    if(isset($request['delete'])){
       $ids =  $request['action'];   
        $email=   enduser::select('email','name')->where('id',$request['action'][0])->get(); 
// /delete email
$iddc =   Auth::user()->id;
$dataw = DB::table('dctemps')->where('name','deleteeu')->where('dcid', $iddc)->get();
$emailn = $email[0]->name;
$emailw = $email[0]->email;
Mail::send('dcsection.emails.dcmail', ['user'=>$emailn,'password'=>'&nbsp','email'=>'&nbsp','datecontract'=>'&nbsp','Template'=>$dataw[0]->tmp,], function ($m) use ($emailw,$request) {
$m->from('virenderminhas22@gmail.com', 'LA HUP');

$m->to($emailw, 'test')->subject('HUP ');

});

foreach ($ids as $key => $value) {

enduser::where('id',$value)->delete();

$key++;
}
foreach ($ids as $key => $value1) {
user::where('id',$value1)->delete();
$key++;
}
}

    

// ////////Inactive section///////////////////
   if(isset($request['inactive'])){

    
//  /////inactive email//
if(!empty($request->sendemail)){

  $email=   enduser::select('email','name')->where('id',$request['action'][0])->get();
       $emailn = $email[0]->name;
 $emailw = $email[0]->email;
$iddc =   Auth::user()->id;
$dataq = DB::table('dctemps')->where('name','inactiveeu')->where('dcid', $iddc)->get();

      Mail::send('dcsection.emails.dcmail', ['user'=>$emailn,'password'=>'&nbsp','email'=>'&nbsp','datecontract'=>'&nbsp','Template'=>$dataq[0]->tmp,], function ($m) use ($emailw ,$request) {
$m->from('virenderminhas22@gmail.com', 'LA HUP');

$m->to($emailw, 'test')->subject('HUP ');

});
    }


        $ids =  $request['action'];
       foreach ($ids as $key => $value) {
       enduser::where('id',$value)->update([
'status'=>'0',
       ]);
       $key++;
       }

 user::where('id',$request['action'][0])->update([
'status'=> '0',
 ]);
   }

/////////////end inactive section////////////////
   /////////////start active section////////////////

   if(isset($request['active'])){


//////active v////////
if(!empty($request->sendemail)){
  
$email=   enduser::select('email','name')->where('id',$request['action'][0])->get();
       $emailn = $email[0]->name;
 $emailw = $email[0]->email;
$iddc =   Auth::user()->id;
$dataq = DB::table('dctemps')->where('name','activeeu')->where('dcid', $iddc)->get();

      Mail::send('dcsection.emails.dcmail', ['user'=>$emailn,'password'=>'&nbsp','email'=>'&nbsp','datecontract'=>'&nbsp','Template'=>$dataq[0]->tmp,], function ($m) use ($emailw ,$request) {
$m->from('virenderminhas22@gmail.com', 'LA HUP');

$m->to($emailw, 'test')->subject('HUP ');

});
    }

            $ids =  $request['action'];
       foreach ($ids as $key => $value) {
       enduser::where('id',$value)->update([
'status'=>'1',
       ]);
       $key++;
       }
      
       user::where('email',$email[0]['email'])->update([
'status'=> '1',
 ]);

   }
        return redirect('/dcsection/EnduserManagement')->with('success','Your Action Successfully Complete');
    }
       public function savevm(Request $request)
    {

      $request->validate([
  'name'=>'required',
  'uid'=>'required',
  'procs'=>'required',
    'host'=>'required',
  'vm'=>'required',
  'cores'=>'required',
  'dateinstall'=>'required',
      ]);
$now = new DateTime();
Vminventory::create([
'name'=>$request['name'],
'user_id'=>$request['uid'],
'dc_id'=>Auth::user()->id,
'host'=>$request['host'],
'endUser'=>$request['name'],
'updatedate'=>$now,
'vm'=>$request['vm'],
'cores'=>$request['cores'],
'procs'=>$request['procs'],
'dateinstall'=>$request['dateinstall'],
'sql'=>'',
'office'=>'',
'sharepoint'=>'',
'exchange'=>'',
'skypebiz'=>'',
'visualstudio'=>'',
'dynamics'=>'',
'project'=>'',
'rds'=>'',
'visio'=>'',
  ]);

        return redirect('dcsection/home')->with('success','successfully Add VM ');

    }
        public function show(Request $request)
    {

    $idd =   Auth::user()->id;
    $vmdata['vmdata']   = Vminventory::where('dc_id',$idd)->orderby('user_id','desc')->get();
    $idddd = Auth::user()->id;

$vmdata['count']= notifications::where('dcid',$idddd)->where('status',0)->count();

$vmdata['notidataheader']= notifications::where('dcid',$idddd)->where('status',0)->where('parent','qns')->get();


$vmdata['notidata']= notifications::where('dcid',$idddd)->get();

       $vmdata['distinct']   = Vminventory::where('dc_id',$idd)->select('user_id')->get();
         $vmdata['name'] =  enduser::select('name','id')->where('dc_id',$idd)->where('status','1')->get();

         // dd($vmdata['name']);
           $datel  = date('Y-m-d H:i:s');
    $vmdata['date'] =   date('Y-m-d H:i:s',strtotime('+2 minutes',strtotime($datel)));


    return view('dcsection/home',$vmdata);
    }
    // 
 


//
public function search(Request $request)
{
if($request->ajax())
{

$products=enduser::where('id',$request->search)->get();

if($products[0]->status == '0' ){
$output['inactive'] = '<button  name="active"type="submit" class="btn grey-salt  btndisable pl-5 pr-5">Yes </button>';
$output['activeheading'] = '<span><b>Active</b></span>';
}
if($products[0]->status == '1' ){
$output['inactive'] = '<button  name="inactive"type="submit" class="btn grey-salt  btndisable pl-5 pr-5">Yes </button>';
$output['activeheading'] = '<span><b>Inactive</b></span>';
}
if($products)
{  
foreach ($products as $key => $product) {
$output['output']  ='<input type="hidden"name="id"value="'.$product->id.'"/><div class="input-group"style="margin-bottom:8px">
<span class="input-group-addon">
<i class="fa fa-user"></i>
</span>
<input type="text" class="form-control" value="'.$product->name.'"placeholder="Enter Username"name="username"> </div>
   
   <div class="input-group"style="margin-bottom:8px">
<span class="input-group-addon">
<i class="fa fa-lock fa-fw"></i>
</span>
<input type="password" class="form-control" value="'.$product->password.'"placeholder="Enter password"name="password"> </div>  

<div class="input-group"style="margin-bottom:8px">
<span class="input-group-addon">
<i class="fa fa-envelope"></i>
</span>
<input type="email" class="form-control"value="'.$product->email.'" placeholder="Email Address"name="email"> </div>
<div class="input-group"style="margin-bottom:8px">
<span class="input-group-addon">
<i class=" icon-layers d"></i>
</span>
<input type="text" class="form-control"value="'.$product->typecontract.'" placeholder="Enter Type contract name"name="typecontract"> </div>
<div class="input-group"style="margin-bottom:8px">
<span class="input-group-addon">
<i class="icon-calendar"></i>
</span>
<input type="date" class="form-control"value="'.$product->datecontract.'" placeholder=""name="datecontract"> </div>




<div class="input-group"style="margin-bottom:8px">
<span class="input-group-addon">
<i class="fa fa-sticky-note-o"></i>
</span>
<textarea type="text" class="form-control" value=""placeholder="comment"name="comment">'.$product->comment.'</textarea> </div>
<tr>

<td><input type="checkbox" class="" value="sendmail"name="sendemail"> Notify EU by E-mail</td>
<td></td>
</tr><br><br>
<tr>
    <td>
<input type="submit" class="btn btn-info circle"value="Submit"> &nbsp&nbsp</td>
<td>
<input data-dismiss="modal" aria-label="Close" type="button" class="btn grey-salt circle"value="Cancel"> 
</td>
</tr>
';
}

//
// 
foreach ($products as $key => $product) {

$output['mail'] =' <table class="table table-responsive"style="overflow:auto!important;">
<tr >
  <td>EU Name<input type="hidden" name="name"value="'.$product->name.'">  </td>
  <td>:</td>
  <td>'.$product->name.'</td>
</tr>
       
<tr>
  <td>Contract Name  <input type="hidden" name="cname"value="'.$product->typecontract.'"></td>
    <td>:</td>
  <td>'.$product->typecontract.'</td>
</tr>

<tr>
  <td>Contract Date <input type="hidden" name="cdate"value="'.$product->datecontract.'"> </td>
    <td>:</td>
  <td>'.$product->datecontract.'</td>
</tr>
<tr>
  <td>Comment <input type="hidden" name="cdate"value="'.$product->comment.'"> </td>
    <td>:</td>
  <td>'.$product->comment.'</td>
</tr>

<tr>
  <td>Email <input type="hidden" name="email"value="'.$product->email.'"> </td>
    <td>:</td>
  <td>'.$product->email.'</td>
</tr>
<tr>
  <td colspan="3"></td><td><center><input type="submit" class="btn btn-lg btn-info circle p-5" name=""value="&nbsp&nbsp&nbsp&nbsp&nbsp Send &nbsp&nbsp&nbsp&nbsp&nbsp"></center></td>
</tr>
</table>';

}
return Response($output);
   }
   }
}

    // 

 

  public function update(Request $request)
    {

$ids = $request['id'];


   $data = $request->all();
        $checkue = user::where('id',$ids)->update([
'name'=>$data['username'],
'email'=>$data['email'],
'password'=>bcrypt($data['password']),
'status'=>'1',
        ]); 

       $data = $request->all();
        $check = enduser::where('id',$ids)->update([
         
'name'=>$data['username'],
'email'=>$data['email'],
'typecontract'=>$data['typecontract'],
'datecontract'=>$data['datecontract'],
'comment'=>$data['comment'],
'password'=>bcrypt($data['password']),
'status'=>'1',
        ]);     
if(!empty($request->sendemail)){
$dcid = Auth::user()->id;
$email = $request->email;
$password = $request->password;
$datecontract = $request->datecontract;
$user = $request->username;

$data= DB::table('dctemps')->where('name','editeu')->where('dcid',$dcid)->get();
   
      Mail::send('dcsection.emails.dcmail', ['user' => $user,'password' => $request->password,'datecontract' => $request->$datecontract,'email' => $request->email,'Template'=>$data[0]->tmp,], function ($m) use ($email,$request) {
$m->from('virenderminhas22@gmail.com', 'LA HUP');

$m->to($email, 'test')->subject('HUP ');

});
    }

        return redirect('dcsection/EnduserManagement')->with('success','user successfully updated ');
    }

    // ssssss


function import(Request $request) {

  $ldate = date('Y-m-d ');
// dd($ldate);

  $request->validate([
  'file'=>'required',
  ]);
          $file = $_FILES['file']['tmp_name'];
          $handle = fopen($file, "r");

		  /*-------------------*/
		  $delimiters = array(
        ';' => 0,
        ',' => 0,
        "\t" => 0,
        "|" => 0
    );

   // $handle = fopen($csvFile, "r");
    $firstLine = fgets($handle);
    //fclose($handle); 
    foreach ($delimiters as $delimiter => &$count) {
        $count = count(str_getcsv($firstLine, $delimiter));
    }

     $abc =  array_search(max($delimiters), $delimiters);
		   
		   /*-------------------*/
		 

$filesop = fgetcsv($handle, 1000, $abc);
           
                      // dd($filesop);
                      $c = 0;
                      // dd($filesop[1]);
  if($filesop[1] =='poweredOff'){
while(($filesop = fgetcsv($handle, 1000, $abc)) !== false)
{

   if($c >= 1 && count($filesop)>1){

                      // dd($filesop[1]);
       $vm = $filesop[0];
       $powestate = $filesop[1];
       $host = $filesop[2];
       $Cluster =$filesop[3];
       $Datacenter =$filesop[4];
       $OS =$filesop[5];
       $BIOSDate  =$filesop[6];
          $StatusofVM  =$filesop[7];
         $DNSName =$filesop[8];
         $Gueststate =$filesop[9];
        $PowerOn =$filesop[10];
        $VMID =$filesop[11];
        $VMUUID =$filesop[12];
        $vCenterUUID =$filesop[13];
         $vCores =$filesop[14];
        $Sockets =$filesop[15];
        $CoresperSocket =$filesop[16];
         $CreationTime =$filesop[17];

          Vminventory::insert([
          'name'=>'',

'user_id'=>'',
'dc_id'=>Auth::user()->id,
'endUser'=>'',
'host'=>'',
'updatedate'=>$ldate,
'vm'=>$vm,
'host'=>$host,
'cores'=>$vCores,
'procs'=>$CoresperSocket,
'dateinstall'=>$ldate,
'sql'=>'',
'office'=>'',
'sharepoint'=>'',
'exchange'=>'',
'skypebiz'=>'',
'visualstudio'=>'',
'dynamics'=>'',
'project'=>'',
'rds'=>'',
'visio'=>'',

          ]);

        

}
     
                     $c++;
}
 }else{

// dd('dssf');
// 2nd type header csv code//
$c = 0;
while(($filesop = fgetcsv($handle, 1000, $abc)) !== false)
{

// echo"<pre>";
// echo  print_r($filesop);
// echo"</pre>";

   if($c >= 1 && count($filesop)>1){

                      // dd($filesop[1]);
        $vm = $filesop[0];
           // $powestate = $filesop[1];
		  
       $host = $filesop[1];
       $Cluster =$filesop[2];
       $Datacenter =$filesop[3];
       $OS =$filesop[4];
       $BIOSDate  =$filesop[5];
       $StatusofVM  =$filesop[6];
       $DNSName =$filesop[7];
        // $Gueststate =$filesop[9];
       $PowerOn =$filesop[8];
       $VMID =$filesop[9];
       $VMUUID =$filesop[10];
       $vCenterUUID =$filesop[11];
       $vCores =$filesop[12];
       $Sockets =$filesop[13];
       $CoresperSocket =$filesop[14];
       $CreationTime =$filesop[15];

          Vminventory::insert([
          'name'=>'',

'user_id'=>'',
'dc_id'=>Auth::user()->id,
'endUser'=>'',
'host'=>'',
'updatedate'=>$ldate,
'vm'=>$vm,
'host'=>$host,
'cores'=>$vCores,
'procs'=>$CoresperSocket,
'dateinstall'=>$ldate,
'sql'=>'',
'office'=>'',
'sharepoint'=>'',
'exchange'=>'',
'skypebiz'=>'',
'visualstudio'=>'',
'dynamics'=>'',
'project'=>'',
'rds'=>'',
'visio'=>'',

          ]);

        

} //endif $c row
     
                     $c++;
}   //endwhile


// end//
           } //else end


            return redirect('dcsection/home');
   

}

// 
function exportdata(Request $request){
 
  $data = Vminventory::where('dc_id',$request['dcid'])->get()->toArray();
  /* dd($data);*/
 $filename = "Export_excel.xls";
    header("Content-Type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=\"$filename\"");
    $isPrintHeader = false;

    if (! empty($data)) {
        foreach ($data as $row) {

            if (! $isPrintHeader) {
                echo implode("\t", array_keys($row)) . "\n";
                $isPrintHeader = true;
            }
            echo implode("\t", array_values($row)) . "\n";
        }
    }
    exit();

}

// /////////////////////ajjax noti

    public function ajaxRequestPost(Request $request)

    {

        $input = $request->all();
        $idddd = Auth::user()->id;
$daata= notifications::where('dcid',$idddd)->where('status',0)->count();
$notidataheader= notifications::where('dcid',$idddd)->where('status',0)->where('parent','qns')->get();


$output1 = '';
  foreach($notidataheader as $notidataa){
   $url = url('dcsection/notification', $notidataa->id);

$output1 .='<li><a class="btn" href="'.$url.'">
<span class="time">
'.date("h:i:s Y/M/d",strtotime($notidataa->created_at)).' </span>
<span class="details">
<span class="label label-sm label-icon label-success">
<i class="fa fa-plus"></i>
</span> '.$notidataa->name.' Update . <br>
'. substr($notidataa->massage,0,40).'</span>
</a></li>';


                         }


        return response()->json(['success'=>$daata,'notiaa'=>$output1]);

    }


public function clean(Request $request){

     $idddd = Auth::user()->id;
$daata= Vminventory::where('dc_id',$idddd)->delete();
// dd($request);

  return Redirect('dcsection/home');
}


Public function massage(){
        $idddd = Auth::user()->id;
$daata= notifications::where('dcid',$idddd)->where('status',0)->count();
$daatala= notifications::where('dcid',$idddd)->where('status',22)->count();
$notidataheader= notifications::where('dcid',$idddd)->where('status',0)->where('parent','qns')->get();

$lanoti= notifications::where('dcid',$idddd)->where('parent','op')->get();

return view('dcsection.lanoti',['count'=>$daata,'notidataheader'=>$notidataheader,'lanoti'=>$lanoti,'daatala'=>$daatala]);

}

public function sendlamassage(Request $request){

// dd($request);
notifications::create([
'name'=>Auth::user()->name,
'dcid'=>Auth::user()->id,
'userid'=>'la',
'massage'=>$request['massage'],
'nfrom'=>'dc',
'nto'=>'la',
'parent'=>'op',
'status'=>'19',
]);


return redirect('/dcsection/lanoti')->with('success','successfully Send Message ');
}

public function deletelasms($id){

 // dd($request);
notifications::where('id',$id)->delete();


return back()->with('success','successfully Send Message ');
}
   
}








