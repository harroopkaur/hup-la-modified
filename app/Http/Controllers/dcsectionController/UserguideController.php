<?php

namespace App\Http\Controllers\dcsectionController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Category;
use Illuminate\Support\Facades\Validator;
use App\notifications;
use Auth;
use App\enduser;
use App\User;
class UserguideController extends Controller
{
    public function __construct()
    {
         $this->middleware('dccheckstatus'); 
        $this->middleware('dcsection'); 
        View::share(['page_name_active'=> 'dcsection/category']);
    }
    public function cat_list()
    {
        $idddd = Auth::user()->id;
        $data['count']= notifications::where('dcid',$idddd)->where('status',0)->count();
        $data['notidataheader']= notifications::where('dcid',$idddd)->where('status',0)->where('parent','qns')->get();
        $data['category']=Category::get();
        return view('dcsection.category.allcategory',$data);
    }
    /* public function addcategory()
    {   
        return view('dcsection.category.addcategory');
    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required',  'max:255'],
            'content' => ['required'],
            
        ]);
    }
    public function insert_value(Request $request)
    {   
        $this->validator($request->all())->validate();
        
        Category::create([
            'name' => $request['name'],
            'description' => $request['content'],
            
        ]);
        return back()->with('success', 'You Have Submit Data');
    }

     public function editcategory($id)
    {
        $data['category']=Category::where('id',$id)->get();
        return view('dcsection.category.editcategory',$data);
    }
    public function update(Request $request)
    {
        $this->validator($request->all())->validate();
        Category::where('id',$request['id'])->update([

            'name' => $request['name'],
            'description' => $request['content'],
            ]);
            return back()->with('success', 'You Have Update Data');
    }
    public function deletecategory($id)
    {
        Category::where('id',$id)->delete();
        return back()->with('success', 'You Have Delete Data');
    }*/

}
