<?php

namespace App\Http\Controllers\dcsectionController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Resources_manage;
use View;
use App\Category;
use App\notifications;
use Auth;
use App\enduser;
use App\User;
class manageresourcesController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
         $this->middleware('dccheckstatus'); 
        $this->middleware('dcsection');
        View::share(['page_name_active'=> 'dcsection/resources']);
    }
    public function resources($id)
    {
      $idddd = Auth::user()->id;
        $data['count']= notifications::where('dcid',$idddd)->where('status',0)->count();
        $data['notidataheader']= notifications::where('dcid',$idddd)->where('status',0)->where('parent','qns')->get();
        $data['id']=$id;
        $data['image']=Resources_manage::where('type','image')->where('cat_id',$id)->paginate(18);
        $data['video']=Resources_manage::where('type','video')->where('cat_id',$id)->paginate(18);
        $data['pdf']=Resources_manage::where('type','pdf')->where('cat_id',$id)->paginate(18);
        $data['doc']=Resources_manage::where('type','doc')->where('cat_id',$id)->paginate(18); 
        $data['cat']=Category::where('id',$id)->get();
        return view('dcsection.manage_resources.show_list_resources', $data);
    }
    public function addresources($id)
    {
        $data['cat_id']=$id;
        $data['cat']=Category::where('id',$id)->get();
        return view('dcsection.manage_resources.add_resources',$data);
    }
    public function insert_data(Request $request)
    {

      	$file= $request['image'];
      
        $file_image = str_replace([' ', ':'], '-',date('Y-m-d').'-'.date("h-i-s-a")).'-' .$file->getClientoriginalName();
          $path= base_path().'/public/img/';
          $file->move($path,$file_image);

          Resources_manage::create([
            'image' => $file_image,
            'title' => $request['title'],
            'cat_id' => $request['category'],
            'content' => $request['content'],
            'type'  =>   $request['type'],
            
          ]);
          
          return redirect()->route('dcsection.resources', ['id'=>$request['category']])->with('success', 'You Have Submit Data');
    }
    public function delete(Request $request){
      $data=[];
      $data=$request->check;
      
     $count=count($request->check);

      if($data==""){
        return back()->with('success' ,'Please Select The Values');
      }
      else{
        
     for($i=0; $i<$count; $i++ ){
         
      Resources_manage::where('id',$data[$i])->delete();
     }
     return back()->with('success', ' You Have Delete Data');
    }
    
  
  }
}
