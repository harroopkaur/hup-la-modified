<?php

namespace App\Http\Controllers;
use App\Http\Controllers\dccontroller;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use view;
use App\dc;
use App\Dcsection;
use validator;
use App\Vminventory;
use App\tmp;
use DateTime;
use Mail;
use DB;
use App\dctemp;
use App\dctmpforeus;
use App\notifications;
use Auth;

class dccontroller extends Controller
{
   public function __construct()
    {
        $this->middleware('admin');  
    }

    // massagesDC aLL///
public function  dcmassage(){
$alldc['datadc'] =DB::table('dcsections')->get();

  return view('admin.massage',$alldc);
}
    // END//
// notiadmin //

    public function notiview(){

$dc['dcs'] = Dcsection::get();

// notifications::where('id',$id)->where('status','0')->update([
//   'status'=>'1',
// ]);
// notifications::where('parent',$id)->where('status','0')->update([
//   'status'=>'1',
// ]); 
// $idddd = Auth::user()->id;
// // dd($idddd);
// $vmdata['count']= notifications::where('dcid',$idddd)->where('status',0)->count();
// $vmdata['notidata']= notifications::where('dcid',$idddd)->where('id',$id)->get();
// $vmdata['notidataheader']= notifications::where('dcid',$idddd)->where('status',0)->where('parent','qns')->get();
// $vmdata['distinct']   = Vminventory::where('dc_id',$idddd)->select('user_id')->get();
// // $vmdata['name'] =  User::select('name','id')->get();
// $datel  = date('Y-m-d H:i:s');
// $vmdata['date'] =   date('Y-m-d H:i:s',strtotime('+2 minutes',strtotime($datel)));
// $vmdata['chatparent']= notifications::where('id',$id)->get();
// $vmdata['conversation']= notifications::where('parent',$id)->get();

   return view('admin.noti',$dc);

  
}
    // 
public function tmpview(){
$tmpdata['tmp'] = tmp::get();
$tmpdata['dctmp'] = dctmpforeus::get();
return view('admin.tmpview',$tmpdata);
}




    //////////
  public function sendmail(Request $request)
{

$email = $request->email;
$password = $request->password;
$company = $request->company;
$user = $request->name;
 $data = tmp::where('id',$request['tmpid'])->get();

      Mail::send('admin.emails.dcmail', ['user' => $request->name,'password' => $request->password,'company' => $request->company,'email' => $request->email,'Template'=>$data[0]['tmp'],], function ($m) use ($request) {
$m->from('virenderminhas22@gmail.com', 'LA HUP');

$m->to($request->email, 'test')->subject('Data Center Login');

});


return redirect('/admin/index')->with('success','Successfully Send Mail');

   }

    // /
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
  $data['data'] = dc::orderby('status')->get();
 
  return view('admin.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updatevm(Request $request)
    {

            request()->validate([
  'name'=>'required',
  'uid'=>'required',
  'procs'=>'required',
  'vm'=>'required',
  'cores'=>'required',
  'dateinstall'=>'required',
      ]);
            if($request['sql']==''){
$sql='';
            }else{
           $sql=$request['sql'];   
            }

             if($request['office']==''){
$office='';
            }else{
              $office = $request['office'];
            }
             if($request['sharepoint']==''){           
$sharepoint='';
            }else{

  $sharepoint = $request['sharepoint'];      
            }
             if($request['exchange']==''){
$exchange='';
            }else{
            $exchange=$request['exchange'];  
            }


             if($request['skypebiz']==''){
$skypebiz='';
            }else{
          $skypebiz=$request['skypebiz'];    
            }
             if($request['visualstudio']==''){
            
$visualstudio='';
            }else{

$visualstudio=$request['visualstudio'];    
            }
             if($request['dynamics']==''){
            
$dynamics='';
            }else{
              $dynamics=$request['dynamics'];
            }
             if($request['project']==''){
            
$project='';
            }else{

$project=$request['project'];  
            }
             if($request['rds']==''){
            
$rds='';
            }else{
$rds=$request['rds'];
            }
             if($request['visio']==''){
            
$visio='';
            }else{
              $visio=$request['visio'];
            }
         $id =  $request['uid'];
$now = new DateTime();
Vminventory::where('id',$id)->update([
'name'=>$request['name'],
'user_id'=>'',
'dc_id'=>'',
'endUser'=>$request['name'],
'updatedate'=>$now,
'vm'=>$request['vm'],
'cores'=>$request['cores'],
'procs'=>$request['procs'],
'dateinstall'=>$request['dateinstall'],
'sql'=>$sql,
'office'=>$office,
'sharepoint'=>$sharepoint,
'exchange'=>$exchange,
'skypebiz'=>$skypebiz,
'visualstudio'=>$visualstudio,
'dynamics'=>$dynamics,
'project'=>$project,
'rds'=>$rds,
'visio'=>$visio,
  ]);

        return redirect('admin/vminventory')->with('success','successfully Update VM ');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function store(Request $request)
    {  
      
    request()->validate([
        'name' => 'required|unique:dcs',
        'email' => 'required|unique:dcs',
        'company' => 'required',
          'password' => 'required',
        ]);

        $data = $request->all();
      
        $check = Dcsection::create([
            'name' => $data['name'],
            'email' => $data['email'],
              'status'=>'1',
            'password' => bcrypt($data['password']),
              
        ]);
          $dcid = $check->id;
        $checkq = dc::create([
          'id'=>$dcid,
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
              'rlpassword'=>$data['password'],
                'company'=>$data['company'],
                'status'=>'1',
        ]);
 
   /////////tmp asign dc////


$datas = DB::table('dctmpforeuses')->get();

foreach($datas as $data){
    DB::table('dctemps')->insert(['dcid' => $dcid, 'name'=>$data->name,'tmp'=>$data->tmp]);
}
////////end////
if(!empty($request->sendemail)){
$email = $request->email;
$password = $request->password;
$company = $request->company;
$user = $request->name;
 $data = tmp::where('name','adddc')->get();

      Mail::send('admin.emails.dcmail', ['user' => $user,'password' => $request->password,'company' => $request->company,'email' => $request->email,'Template'=>$data[0]['tmp'],], function ($m) use ($request) {
$m->from('virenderminhas22@gmail.com', 'Data Center Login');

$m->to($request->email, 'test')->subject('HUP');

});
    }
        return redirect('admin/index')->with('success','DC successfully Created ');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

       public function savevm(Request $request)
    {

      request()->validate([
  'name'=>'required',
  'uid'=>'required',
  'procs'=>'required',
  'vm'=>'required',
  'cores'=>'required',
  'dateinstall'=>'required',
      ]);
$now = new DateTime();
Vminventory::create([
'name'=>$request['name'],
'user_id'=>$request['uid'],
'dc_id'=>'',
'endUser'=>$request['name'],
'updatedate'=>$now,
'vm'=>$request['vm'],
'cores'=>$request['cores'],
'procs'=>$request['procs'],
'dateinstall'=>$request['dateinstall'],
'sql'=>'',
'office'=>'',
'sharepoint'=>'',
'exchange'=>'',
'skypebiz'=>'',
'visualstudio'=>'',
'dynamics'=>'',
'project'=>'',
'rds'=>'',
'visio'=>'',
  ]);

        return redirect('admin/vminventory')->with('success','successfully Add VM ');

    }


    public function show(Request $request)
    {
    $vmdata['vmdata']   = Vminventory::whereRaw('name <> ""')->orderby('name','desc')->get();

    $vmdata['test']   = Vminventory::distinct('user_id')->get();

       $vmdata['distinct']   = Vminventory::select('user_id')->distinct()->get();

    return view('admin/vminventory',$vmdata);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
 $data = $request->all();
        $check = dc::where('email',$request->email)->update([
            'name' => $data['name'],
            'email' => $data['email'],
            'company'=>$request->company,
                'status'=>'1',
        ]);
   $check = Dcsection::where('email',$request->email)->update([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
             'status'=>'1',
              
        ]);

if(!empty($request->sendemail)){
$email = $request->email;
$password = $request->password;
$company = $request->company;
$user = $request->name;
 $data = tmp::where('name','editdc')->get();

      Mail::send('admin.emails.dcmail', ['user' => $user,'password' => $request->password,'company' => $request->company,'email' => $request->email,'Template'=>$data[0]['tmp'],], function ($m) use ($request) {
$m->from('virenderminhas22@gmail.com', 'Updated Account');

$m->to($request->email, 'test')->subject('Updated Account ');

});
    }
       
        return redirect('admin/index')->with('success','DC successfully updated ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function action(Request $request)
    {
   
//$pass =   dc::select('password')->where('id',$request['action'])->get();

   
///delete email
if(!empty($request->sendemail)){

 $data = tmp::where('name','deletedc')->get();
   $email =   dc::select('email','name')->where('id',$request['action'])->get();
$emaildel = $email[0]['email'];
$namedel = $email[0]['name'];
// dd($data);
      Mail::send('admin.emails.dcmail', ['user' => $namedel,'password' => '@@@@@','company' => '@@@@@','email' => '@@@@@','Template'=>$data[0]->tmp,], function ($m) use ($emaildel,$request) {
$m->from('virenderminhas22@gmail.com', 'Delete Account');

$m->to($emaildel, 'test')->subject('Delete Account ');

});
    }

  if(isset($request['delete'])){
       $ids =  $request['action'];
       foreach ($ids as $key => $value) {
       dc::where('id',$value)->delete();

       $key++;
       }
         foreach ($ids as $key => $value1) {
dcsection::where('id',$value1)->delete();
$key++;
}

   } 

// ////////Inactive section///////////////////
  if(isset($request['inactive'])){    

if(!empty($request->sendemail)){

  $email=   dc::select('email','name')->where('id',$request['action'][0])->get();
       $emailn = $email[0]->name;
 $emailw = $email[0]->email;

$dataq = tmp::where('name','inactivedc')->get();

      Mail::send('admin.emails.dcmail', ['user'=>$emailn,'password'=>'&nbsp','email'=>'&nbsp','company'=>'&nbsp','Template'=>$dataq[0]->tmp,], function ($m) use ($emailw ,$request) {
$m->from('virenderminhas22@gmail.com', 'Inactive Account');

$m->to($emailw, 'test')->subject('Inactive Account ');

});
    }


        $ids =  $request['action'];
       foreach ($ids as $key => $value) {
       dc::where('id',$value)->update([
'status'=>'0',
       ]);
       $key++;
       }

 dcsection::where('id',$request['action'][0])->update([
'status'=> '0',
 ]);
   }


/////////////end inactive section////////////////
   /////////////start active section////////////////
// ////////Inactive section///////////////////
  if(isset($request['active'])){    

if(!empty($request->sendemail)){

  $email=   dc::where('id',$request['action'][0])->get();
       $emailn = $email[0]->name;
 $emailw = $email[0]->email;
$company = $email[0]->company;
$password = $email[0]->password;
$dataq = tmp::where('name','activedc')->get();

      Mail::send('admin.emails.dcmail', ['user'=>$emailn,'password'=>$password,'email'=>$emailw,'company'=>$company,'Template'=>$dataq[0]->tmp,], function ($m) use ($emailw ,$request) {
$m->from('virenderminhas22@gmail.com', 'Active Account');

$m->to($emailw, 'test')->subject('Active Account ');

});
    }


        $ids =  $request['action'];
       foreach ($ids as $key => $value) {
       dc::where('id',$value)->update([
'status'=>'1',
       ]);
       $key++;
       }

 dcsection::where('id',$request['action'][0])->update([
'status'=> '1',
 ]);
   }


/////////////end inactive section////////////////



// //////////////////////////////

        return redirect('/admin/index')->with('success','Your Action Successfully Complete');
    }



public function search(Request $request)
{
if($request->ajax())
{
   $data = tmp::get();
$products=dc::where('id',$request->search)->get();


if($products[0]->status == '0' ){
$output['inactive'] = '<button  name="active"type="submit" class="btn grey-salt  btndisable pl-5 pr-5">Yes </button>';
$output['activeheading'] = '<span><b>Active</b></span>';
}
if($products[0]->status == '1' ){
$output['inactive'] = '<button  name="inactive"type="submit" class="btn grey-salt  btndisable pl-5 pr-5">Yes </button>';
$output['activeheading'] = '<span><b>Inactive</b></span>';
}

if($products)
{  
foreach ($products as $key => $product) {
$passw = '******';
$output['output'] ='<input type="hidden"name="id"value="'.$product->id.'"/><div class="input-group editdcform"style="margin-bottom:8px"id="editdcform"><span class="input-group-addon"><i class="fa fa-user"></i></span><input type="text" class="form-control" placeholder="Enter Username"name="name"value="'.$product->name.'"required> </div>
<div class="input-group"style="margin-bottom:8px">
<span class="input-group-addon">
<i class="fa fa-lock fa-fw"></i>
</span>
<input type="password"value="'.$passw.'" class="form-control" placeholder="Enter password"name="password"required> </div>  
<div class="input-group"style="margin-bottom:8px">
<span class="input-group-addon">
<i class="fa fa-user"></i>
</span>
<input type="text" class="form-control"value="'.$product->company.'" placeholder="Enter company name"name="company"required> </div>
<div class="input-group"style="margin-bottom:8px">
<span class="input-group-addon">
<i class="fa fa-envelope"></i>
</span>
<input type="email" class="form-control"value="'.$product->email.'" placeholder="Email Address"name="email"required> </div>
<tr><td><input type="checkbox"name="sendemail"value="sendmail"/><span>&nbspNotify Dc by E-mail</span><td></tr><br></br>

<tr>
    <td>
<input type="submit" class="btn btn-info circle"value="Update"> &nbsp&nbsp</td>
<td>
<input data-dismiss="modal" aria-label="Close" type="button" class="btn grey-salt circle"value="Cancel"> 
</td>
</tr>
';

}
// //
$tmp = '';
foreach($data as $data1){
               $tmp .= '<option value="'.$data1->id.'">'.$data1->name.'</option>';

              }
////
foreach ($products as $key => $product) {
$passq = $product->rlpassword;
$output['mail'] =' <table class="table table-responsive"style="overflow:scroll!important;">
<tr>
<td colspan="4" >
<select class=" btn btn-info form-control"name="tmpid"required>
          <option value="">Select Template</option>
          '.$tmp.'
        </select>
  </td>
</tr>
<tr >

  <td>DC Name<input type="hidden" name="name"value="'.$product->name.'">  </td>
  <td>:</td>
  <td>'.$product->name.'</td>
</tr>

<tr>
  <td>Company  <input type="hidden" name="company"value="'.$product->company.'"></td>
    <td>:</td>
  <td>'.$product->company.'</td>
</tr>
<tr>
  <td>E-mail <input type="hidden" name="email"value="'.$product->email.'"> </td>
    <td>:</td>
  <td>'.$product->email.'</td>
</tr>
<tr>
  <td colspan="3">

  </td><td><center><input type="submit" class="btn btn-lg btn-info circle p-5" name=""value="&nbsp&nbsp&nbsp&nbsp&nbsp Send &nbsp&nbsp&nbsp&nbsp&nbsp"></center></td>
</tr>
</table>';

}


return Response($output);
   }

}

}
public function tempup($id){
  $tmpdata['tmpup'] = tmp::where('id',$id)->get();
$tmpdata['tmp'] = tmp::get();
return view('admin.tmpup',$tmpdata);
}



public function temp(){
$tmpdata['tmp'] = tmp::get();
// dd($tmpdata['tmp']);
  return view('admin.tmp',$tmpdata,['hello'=>'dsfsfsfs']);
}
public function tempsave(Request $request){
 request()->validate([
  'name'=>'required',
  'tmp'=>'required',

      ]);

dctmpforeus::create([
'name'=>$request['name'],
'tmp'=>$request['tmp'],

]);

return redirect('admin/temp');
}

public function tmpupdate(Request $request){

$idt = $request['idt'];
  $tmpdata['tmpup'] = tmp::where('id',$idt)->update([
'name'=>$request['name'],
'tmp'=>$request['tmp'],
  ]);

return redirect('admin/tmpview');
}


public function tmpupdatedc(Request $request){

$idt = $request['idt'];
  $tmpdata['tmpup'] = dctmpforeus::where('id',$idt)->update([
'name'=>$request['name'],
'tmp'=>$request['tmp'],
  ]);

return redirect('admin/tmpview');
}

public function tempupdc($id){
  // dd($id);
  $tmpdata['tmpup'] = dctmpforeus::where('id',$id)->get();
return view('admin.updatetmpdc',$tmpdata);
}


//chatbox functions///////////////////////////////////////////////////////
public function  chatbox(Request $request){


$explod = explode(',',$request->dcname);
$dcname = $explod[0];
$dcid = $explod[1];

notifications::create([

'name'=>'LA',
'dcid'=>$dcid,
'userid'=>'null',
'massage'=>$request->massage,
'parent'=>'op',
'nfrom'=>'la',
'nto'=>'dc',
'status'=>'22',
]);
return redirect()->to('admin/converladc/'.$dcid)->with('message', 'Successfully Massage Send  ');


}




// ////////////////////////End chatboxc section/////////////////
//////////////////header noti admin /////////
public function converdc($id){
// dd('sdasd');
$lanoti = notifications::where('dcid',$id)->where('parent','op')->get();
return view('admin.converladc',['lanoti'=>$lanoti]);
}
public function lasendsms(Request $request){
// dd($request);
 notifications::create([

'name'=>'LA',
'dcid'=>$request->dcid,
'userid'=>'null',
'massage'=>$request->massage,
'parent'=>'op',
'nfrom'=>'la',
'nto'=>'dc',
'status'=>'22',
]);

  return redirect()->back();
}
///////////////end noti section////////////////////

public function deletesms( $id){
notifications::where('id',$id)->delete();
 // dd($id);

  return back();
}


}




   


   