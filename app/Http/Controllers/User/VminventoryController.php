<?php

namespace App\Http\Controllers\User;
use View;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Hesto\MultiAuth\Traits\LogsoutGuard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered; 	
use Illuminate\Support\Facades\Route;
use App\Vminventory;	
use App\User;
use Mail;
use App\MyTestMail;
use App\Machines;
use App\notifications;
use App\dc;
use Redirect;
use Session;           



class VminventoryController extends Controller
{
			public function __construct()
			{		  
		 $this->middleware('checkstatus');  
       $this->middleware('user'); 
	View::share(['page_name_active'=> 'user/vminventory']);    
            }
public function vminventory(){
$user = session()->get('user_id');

$id=Auth::user()->id;

$data['invent'] = Vminventory::where('user_id',$id)->with('sub_table')->get();
$data[ 'vminvent'] = Machines::get();
$data['count']= notifications::where('userid',$id)->where('status',3)->count();
$data['notidataheader']= notifications::where('userid',$id)->where('status',3)->where('parent','qns')->get();
$data['notidata']= notifications::where('userid',$id)->get();

return view('user.vminventory.vminventory',$data);
}
public function update($id,Request $request){
$data = Vminventory::where('id',$id)->get();

$user['sql']= $request['sql'];
$user['office']= $request['office'];
$user['sharepoint']= $request['sharepoint'];
$user['exchange']= $request['exchange'];
$user['skypebiz']= $request['skypebiz'];
$user['visualstudio']= $request['visualstudio'];
$user['dynamics']= $request['dynamics'];
$user['project']= $request['project'];
$user['rds']= $request['rds'];
$user['visio']= $request['visio'];
//  notifications::create([
// 'name'=>$request[''],
// 'dcid'=>$request[''],
// 'userid'=>$request[''],
// 'vm'=>$request[''],
// 'status'=>'1',
//  ]);
$output = 'SQL :'.$user['sql'].' /OFFICE :'.$user['office'].' /SHAREPOINT :'.$user['sharepoint'].' /EXCHANGE :'.$user['exchange'].' /SKYPEBIZ :'.$user['skypebiz'].' /VISUALSTUDIO :'.$user['visualstudio'].' /DYNAMIC :'.$user['dynamics'].' /PROJECT :'.$user['project'].' /RDS :'.$user['rds'].' /VISION :'.$user['visio'].'';



notifications::insert([
'massage'=>$output,
'dcid'=>$request['dcid'],
'userid'=>Auth::user()->id,
'name'=>Auth::user()->name,
'parent'=>'qns',
'nfrom'=>'user',
'nto'=>'dc',
'status'=>'0',
]);

$email = dc::select('email')->where('id',$request['dcid'])->get();
$em = $email[0]['email'];
Vminventory::where('id',$id)->update([
'sql' => $request['sql'],
'office' => $request['office'],
'sharepoint' => $request['sharepoint'],
'exchange' => $request['exchange'],
'skypebiz' => $request['skypebiz'],
'visualstudio' => $request['visualstudio'],
'dynamics' => $request['dynamics'],
'project' => $request['project'],
'rds' => $request['rds'],
'visio' => $request['visio']
]);

 Mail::send('mail', ['user' => $user], function ($m) use ($user,$em) {
$m->from('virenderminhas22@gmail.com', 'data updated');
$m->to($em, 'data updated ')->subject('Succefully Updated');
});
 if (Mail::failures()) {
        // return response showing failed emails
  return back()->with('success',' Send Information To DC Mail Failures To Send'); 
    }else{
return back()->with('success',' Successfully Update & Send Information To DC');  
} 
}
public function refresh(){

$user = session()->get('user_id');
$id=$user;   
$data['invent'] = Vminventory::where('user_id',$id)->with('sub_table')->get();
$data[ 'vminvent'] = Machines::get();
return view('user.vminventory.refresh',$data); 
}

            // 
public function search(Request $request)
{

  
              if($request->ajax())
              {
              $id = explode(',', $request->search);
              $products=Vminventory::where('id',$request->search)->get();
              if($products)
              {  
              foreach ($products as $key => $product) {
              $output['mail'] ='<span>'.$request->search.'</span>';
              }
              }
              }
return Response($output);
 }
            // 



// ////////////////////////////////////////////////////////////////
 public function notiview($id){
notifications::where('id',$id)->where('status','3')->update([
'status'=>'1',
]);
notifications::where('parent',$id)->where('status','3')->update([
'status'=>'1',
]);
// 
                        $idddd = Auth::user()->id;
                        $vmdata['count']= notifications::where('userid',$idddd)->where('status',3)->count();
                        $vmdata['notidata']= notifications::where('userid',$idddd)->where('id',$id)->get();
                        $vmdata['notidataheader']= notifications::where('userid',$idddd)->where('parent','qns')->where('status',3)->get();
                        $vmdata['name'] =  User::select('name','id')->get();
                        $datel  = date('Y-m-d H:i:s');
                        $vmdata['date'] =   date('Y-m-d H:i:s',strtotime('+2 minutes',strtotime($datel)));



                        $vmdata['chatparent']= notifications::where('id',$id)->get();
                        $vmdata['conversation']= notifications::where('parent',$id)->get();

return view('user.noti',$vmdata);
}


//  public function count()
//  {

// if(Request::ajax()){
//  return 'hellloooooooooo';
   
// }

// }vminventory
 


public function sendmassage(Request $request,$id){

                          notifications::create([
                          'name'=>Auth::user()->name,
                          'dcid'=>$request['dcid'],
                          'userid'=>$request['userid'],
                          'massage'=>$request['massage'],
                          'nfrom'=>'user',
                          'nto'=>'dc',
                          'parent'=>$id,
                          'status'=>'0',
                          ]);

                          notifications::where('id',$id)->update([
                          
                          'status'=>'0',
                          ]);

return redirect()->to('user/notification/'.$id);
}

 
   

    /**

     * Create a new controller instance.

     *

     * @return void

     */

  public function ajaxRequestPost1(Request $request)

    {

        $input = $request->all();
        $idddd = Auth::user()->id;
$daata= notifications::where('userid',$idddd)->where('status',3)->count();
$notidataheader= notifications::where('userid',$idddd)->where('status',3)->where('parent','qns')->get();


$output1 = '';
  foreach($notidataheader as $notidataa){
   $url = url('user/notification', $notidataa->id);

$output1 .='<li style="border-bottom:1px solid black;"><a class="btn" href="'.$url.'">
<span class="time">
'.date("h:i:s Y/M/d",strtotime($notidataa->created_at)).' </span>
<span class="details">
<span class="label label-sm label-icon label-success">
<i class="fa fa-plus"></i>
</span> '.$notidataa->name.' Update . <br>
'. substr($notidataa->massage,0,50).'</span>
</a></li>';


                         }


        return response()->json(['success'=>$daata,'notiaa'=>$output1]);

    }
}