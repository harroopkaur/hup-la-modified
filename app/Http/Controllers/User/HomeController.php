<?php

namespace App\Http\Controllers\User;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Hesto\MultiAuth\Traits\LogsoutGuard;
use Redirect;
use Session;
use View;
use App\notifications;


class HomeController extends Controller
{

public function __construct()
    {
  
     View::share(['page_name_active'=> 'user/home']);
            
    }
    
    public function home(){
//         Auth::user()->id
//           $data['count']= notifications::where('userid',$id)->where('status',3)->count();
// $data['notidataheader']= notifications::where('userid',$id)->where('status',3)->get();
        $users[] = Auth::user();
        $users[] = Auth::guard()->user();
        $users[] = Auth::guard('user')->user();
        Session::put('user_id',Auth::user()->id);
        return view('user.home');
    }
    
}