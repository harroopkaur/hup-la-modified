<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Session;
use URL;
class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
  protected function redirectTo($request)
    {
Session::put('url.intended', URL::full());  
       $rr =  session()->get('url');
        
        //  dd($rr);
        if (! $request->expectsJson()) {           
 if($rr['intended'] === url('admin/vminventory')){

            return route('alogin');

        }elseif($rr['intended'] === url('admin/category')){
             return route('alogin');
             
        }elseif($rr['intended'] === url('dcsection/home')){
             return route('dlogin');
             
        }elseif($rr['intended'] === url('dcsection/EnduserManagement')){
             return route('dlogin');
             
        }elseif($rr['intended'] === url('dcsection/tmpmanage')){
             return route('dlogin');
             
        }elseif($rr['intended'] === url('dcsection/category')){
             return route('dlogin');
             
        }elseif($rr['intended'] === url('admin/tmpview')){
             return route('alogin');
             
        }elseif($rr['intended'] === url('admin/index')){
             return route('alogin');
             
        }elseif($rr['intended'] === url('user/vminventory')){
             return route('elogin');
        }


    }

    // 
    }
}
