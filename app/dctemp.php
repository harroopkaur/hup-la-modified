<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class dctemps extends Authenticatable
{
    //
    protected $fillable = [
        'dcid', 'name', 'tmp',
    ];
}
