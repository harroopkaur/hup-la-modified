<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class enduser extends Model
{
   protected $fillable = [
      'id','name','dc_id', 'email', 'password','typecontract','datecontract','comment','status',
    ];
}
