<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Vminventory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vminventories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('endUser');
            $table->string('updatedate');
            $table->string('vm');
            $table->string('cores');
            $table->string('procs');
            $table->string('sql');
            $table->string('office');
            $table->string('sharepoint');
            $table->string('exchange');
            $table->string('skypebiz');
            $table->string('visualstudio');
            $table->string('dynamics');
            $table->string('project');
            $table->string('rds');
            $table->string('visio');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vminventories');
    }
}
