<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});
Route::get('/', function(){
  return view('site');
});


Route::group(['prefix' => 'admin'], function () {
     Route::get('/', function () {
    return redirect('/admin/login');    
  })->name('alogin');

  Route::get('/', 'AdminAuth\LoginController@showLoginForm')->name('alogin');
  Route::any('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'AdminAuth\RegisterController@register');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'user'], function () {
     Route::get('/', function () {
    return redirect('/user');    
  })->name('elogin');

  Route::get('/', 'UserAuth\LoginController@showLoginForm')->name('elogin');
  Route::any('/login', 'UserAuth\LoginController@login');
  Route::post('/logout', 'UserAuth\LoginController@logout');

  Route::get('/register', 'UserAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'UserAuth\RegisterController@register');

  Route::post('/password/email', 'UserAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'UserAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'UserAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'UserAuth\ResetPasswordController@showResetForm');
  
});



Route::group(['prefix' => 'dcsection'], function () {
     Route::get('/', function () {
    return redirect('/dcsection/login');    
  })->name('dlogin');
  Route::get('/', 'DcsectionAuth\LoginController@showLoginForm')->name('dlogin');
  Route::any('/login', 'DcsectionAuth\LoginController@login');
  Route::post('/logout', 'DcsectionAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'DcsectionAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'DcsectionAuth\RegisterController@register');

  Route::post('/password/email', 'DcsectionAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'DcsectionAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'DcsectionAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'DcsectionAuth\ResetPasswordController@showResetForm');

});